import 'dart:ui';

import 'package:flutter/material.dart';

var myDefaultBackground = Colors.grey[300];

var myAppBar = AppBar(
  title: Text('หน้าหลัก'),
  backgroundColor: Colors.grey[900],
);

var myDrawer = Drawer(
  backgroundColor: Colors.grey[300],
  child: Column(
    children: [
      DrawerHeader(
        child: Image.network(
          'lib/imags/63160005.jpg',
          height: 150,
          width: 150,
          // width: double.infinity,
          // fit: BoxFit.contain,
        ),
      ),
      Text('นาย ทักษ์ติโชค อนุมอญ', style: TextStyle(fontSize: 24)),
      SizedBox(
        height: 30,
      ),
      TextButton(
          onPressed: () {},
          child: Text(
            'หน้าหลัก',
            style: TextStyle(fontSize: 18, color: Colors.black),
          )),
      SizedBox(
        height: 15,
      ),
      TextButton(
          onPressed: () {},
          child: Text(
            'ประวัตืนิสิต',
            style: TextStyle(fontSize: 18, color: Colors.black),
          )),
      // ListTile(
      //   leading: Icon(Icons.chat),
      //   title: Text('M E S S A G E'),
      // ),
      // ListTile(
      //   leading: Icon(Icons.settings),
      //   title: Text('S E T T I N G S'),
      // ),
      // ListTile(
      //   leading: Icon(Icons.info),
      //   title: Text('A B O U T'),
      // ),
      // ListTile(
      //   leading: Icon(Icons.logout),
      //   title: Text('L O G O U T'),
      // ),
    ],
  ),
);
