import 'dart:html';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:midterm_exam/responsive/moblie_Home.dart';
import 'package:midterm_exam/responsive/moblie_Tableclass.dart';
import '../main.dart';
import '../util/utilities.dart';
import '../constants.dart';

class Comment extends StatefulWidget {
  const Comment({Key? key}) : super(key: key);

  @override
  State<Comment> createState() => _CommentState();
}

class _CommentState extends State<Comment> {
  int currentIndex = 1;
  List widfgetOptions = [
    const Tableclass(),
    const Home(),
    const Text('ตารางสอนอาจาร์ย'),
  ];
  String dropdownvalue = 'ไม่ระบุ';
  var items = [
    'ไม่ระบุ',
    'MR.ALAN  JAMIESON',
    'MR.AKHMAD  AMIRUDIN',
  ];
  TextEditingController textarea = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(' เชิญร่วมแสดงความคิดเห็น'),
        backgroundColor: Colors.grey[900],
      ),
      backgroundColor: const Color.fromARGB(255, 235, 236, 217),
      body: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            child: Image.network(
              'lib/imags/reg.png',
              width: double.infinity,
              fit: BoxFit.cover,
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          const Text(
            ' เชิญร่วมแสดงความคิดเห็น',
            style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: Colors.deepOrange),
          ),
          Expanded(
            child: ListView(
              children: [
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Container(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                    ),
                    height: 500,
                    child: Column(
                      // mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Row(
                          children: [
                            const Text(' ศุกร์ 27 มกราคม, 2566',
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold)),
                          ],
                        ),
                        Container(
                          alignment: Alignment.center,
                          padding: const EdgeInsets.all(20),
                          child: Column(
                            children: [
                              TextField(
                                controller: textarea,
                                keyboardType: TextInputType.multiline,
                                maxLines: 4,
                                decoration: const InputDecoration(
                                    hintText: "",
                                    focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            width: 1,
                                            color: Colors.redAccent))),
                              ),
                            ],
                          ),
                        ),
                        Row(
                          children: [
                            const Text(' ต้องการถาม',
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold)),
                            const SizedBox(
                              width: 5,
                            ),
                            DropdownButton(
                              // Initial Value
                              value: dropdownvalue,

                              // Down Arrow Icon
                              icon: const Icon(Icons.keyboard_arrow_down),

                              // Array list of items
                              items: items.map((String items) {
                                return DropdownMenuItem(
                                  value: items,
                                  child: Text(items),
                                );
                              }).toList(),
                              // After selecting the desired option,it will
                              // change button value to selected value
                              onChanged: (String? newValue) {
                                setState(() {
                                  dropdownvalue = newValue!;
                                });
                              },
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            const Text(
                                ' ท่านสามารถระบุข้อความได้สูงสุดไม่เกิน 255 ตัวอักษร',
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold)),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            const Text(' จำนวนตัวอักษร',
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold)),
                            const SizedBox(
                              width: 10,
                            ),
                            const Flexible(
                              child: SizedBox(
                                width: 80.0,
                                height: 50.0,
                                child: TextField(
                                  textAlign: TextAlign.center,
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: '0',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 10.0,
                          width: 50.0,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: ElevatedButton(
                                onPressed: () {
                                  print(textarea.text);
                                },
                                child: const Text("ส่งข้อความ")),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
