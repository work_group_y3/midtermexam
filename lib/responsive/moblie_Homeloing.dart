import 'package:flutter/material.dart';

class Homelogin extends StatelessWidget {
  const Homelogin({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(' ประกาศเรื่อง'),
        backgroundColor: Colors.grey[900],
      ),
      backgroundColor: Color.fromARGB(255, 235, 236, 217),
      body: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            child: Image.network(
              'lib/imags/reg.png',
              width: double.infinity,
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            child: Text(
              'ยินดีต้อนรับเข้าสู่ระบบบริการการศึกษา',
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.deepOrange),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Expanded(
              child: ListView(
            padding: const EdgeInsets.all(8),
            children: <Widget>[
              Container(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                    ),
                    height: 600,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                          width: 400,
                          height: 20,
                          color: Colors.grey,
                          child: Text(" ประกาศเรื่อง",
                              style: TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white)),
                        ),
                        Row(
                          children: [
                            Text(' 1.',
                                style: TextStyle(
                                    fontSize: 10, fontWeight: FontWeight.bold)),
                            Text(
                                ' แบบประเมินความคิดเห็นของนักเรียนและนิสิตต่อการให้บริการของสำนักงานอธิการบดี',
                                style: TextStyle(
                                    fontSize: 9, fontWeight: FontWeight.bold)),
                            Text('(ด่วนที่สุด)',
                                style: TextStyle(
                                    fontSize: 9,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.red)),
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                                '  ขอเชิญนิสิตร่วมทำแบบประเมินความคิดเห็นของนิสิตต่อการให้บริการของ' +
                                    '\n' +
                                    '  สำนักงานอธิการบดี ที่ https://bit.ly/3cyvuuf',
                                style: TextStyle(
                                    fontSize: 10, fontWeight: FontWeight.bold)),
                          ],
                        ),
                        Container(
                          width: 400,
                          height: 10,
                          color: Colors.grey[200],
                          child: Row(
                            children: [
                              Text(" ประกาศโดย",
                                  style: TextStyle(
                                      fontSize: 8,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.deepPurple)),
                              Text(" - วันที่ประกาศ  5 กุมภาพันธ์ 2564",
                                  style: TextStyle(
                                      fontSize: 8,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.indigo)),
                            ],
                          ),
                        ),
                        // SizedBox(
                        //   height: 5,
                        // ),
                        Row(
                          children: [
                            Text(' 2.',
                                style: TextStyle(
                                    fontSize: 10, fontWeight: FontWeight.bold)),
                            Text(' การทำบัตรนิสิตกับธนาคารกรุงไทย',
                                style: TextStyle(
                                    fontSize: 9, fontWeight: FontWeight.bold)),
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                              '   กรณีบัตรหายเสียค่าใช้จ่ายในการทำ 100 บาท' +
                                  '\n' +
                                  '   สำหรับนิสิตรหัส 65 วิทยาเขตบางแสนที่เข้าภาคเรียนที่ 1 ที่ยังไม่รับบัตรให้ติดต่อรับบัตรนิสิตที่ธนาคารกรุงไทย สาขา ม.บูรพา ส่วนนิสิต' +
                                  '\n' +
                                  '   ที่เข้าภาคเรียนที่ 2/2565 รอกำหนดการอีกครั้ง',
                              style: TextStyle(
                                  fontSize: 6,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.red),
                            ),
                          ],
                        ),
                        Image.network(
                          'lib/imags/form.png',
                        ),
                        Container(
                          width: 400,
                          height: 10,
                          color: Colors.grey[200],
                          child: Row(
                            children: [
                              Text(" ประกาศโดย",
                                  style: TextStyle(
                                      fontSize: 8,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.deepPurple)),
                              Text(" - วันที่ประกาศ  5 กุมภาพันธ์ 2564",
                                  style: TextStyle(
                                      fontSize: 8,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.indigo)),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          )),
        ],
      ),
    );
  }
}
