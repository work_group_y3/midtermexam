import 'package:flutter/cupertino.dart';

class Employee {
  final int number;
  final String password;
  final String name;
  final String role;
  final String hourlyRate;

  Employee(this.number, this.password, this.name, this.role, this.hourlyRate);

  static List<Employee> getEmployees() {
    return [
      Employee(1, '63170001', 'นาย ก ก', '3', '2'),
      Employee(2, '63170002', 'นาย ข ข', '3', '2'),
      Employee(3, '63160002', 'นาย ต ต', '3', '2'),
      Employee(4, '63170003', 'นาย ค ค', '3', '2'),
      Employee(5, '63170004', 'นาย ด ด', '3', '2'),
      Employee(6, '63170006', 'นาย ห ห', '3', '2'),
      Employee(7, '63170007', 'นาย ห ห', '3', '2'),
      Employee(8, '63170008', 'นาย ห ห', '3', '2'),
      Employee(9, '63170009', 'นาย ห ห', '3', '2'),
      Employee(10, '63170010', 'นาย ห ห', '3', '2'),
      Employee(11, '63170011', 'นาย ห ห', '3', '2'),
      Employee(12, '63170012', 'นาย ห ห', '3', '2'),
      Employee(13, '63170013', 'นาย ห ห', '3', '2'),
      Employee(14, '63170014', 'นาย ห ห', '3', '2'),
      Employee(15, '63170015', 'นาย ห ห', '3', '2'),
      Employee(16, '63170016', 'นาย ห ห', '3', '2'),
      Employee(17, '63170017', 'นาย ห ห', '3', '2'),
      Employee(18, '63170018', 'นาย ห ห', '3', '2'),
      Employee(19, '63170019', 'นาย ห ห', '3', '2'),
      Employee(20, '63170020', 'นาย ห ห', '3', '2'),
    ];
  }
}

class Registration {
  final String password;
  final String name;
  final String role;
  final String hourlyRate;
  final String team;
  final String grad;

  Registration(this.password, this.name, this.role, this.hourlyRate, this.team,
      this.grad);

  static List<Registration> getEmployees() {
    return [
      Registration('88624359', 'Web Programming', ' GD', '3', '2', ''),
      Registration('88624459', 'Object2Oriented Analysis and Design', 'GD', '3',
          '2', ''),
      Registration('88624559', 'Software Testing', 'GD', '3', '2', ''),
      Registration('88634259', 'Multimedia Programming for Multiplatforms',
          'GD', '3', '2', ''),
      Registration(
          '88634459', '  Mobile Application Development I', 'GD', '3', '2', ''),
      Registration('88646259', 'Introduction to Natural Language Processing',
          'GD', '3', '2', ''),
    ];
  }
}

class PricingDetails extends StatelessWidget {
  final int price;
  const PricingDetails({Key? key, required this.price}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          const Text(
            'ลำดับ:',
            style: TextStyle(fontSize: 36.0),
          ),
          const Spacer(flex: 1),
          Text(
            '$price',
            style: const TextStyle(fontSize: 36.0, fontWeight: FontWeight.bold),
          ),
          const Spacer(flex: 5),
        ],
      ),
    );
  }
}
