import 'package:flutter/material.dart';
import 'package:midterm_exam/responsive/tablet_home.dart';
import '../main.dart';
import '../util/utilities.dart';
import '../constants.dart';

class DesktopScaffold extends StatefulWidget {
  const DesktopScaffold({Key? key}) : super(key: key);

  @override
  State<DesktopScaffold> createState() => _DesktopScaffoldState();
}

class _DesktopScaffoldState extends State<DesktopScaffold> {
  @override
  Widget build(BuildContext context) {
    return TabletScaffold();
  }
}
