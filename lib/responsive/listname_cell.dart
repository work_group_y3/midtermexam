import 'package:flutter/material.dart';

import 'listname.dart';
import 'listname_table.dart';

class ListnameCell extends StatelessWidget {
  final String title;
  final bool isHeader;

  const ListnameCell({
    Key? key,
    required this.title,
    this.isHeader = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: isHeader ? Alignment.center : Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Text(title,
            style: TextStyle(
                fontWeight: isHeader ? FontWeight.bold : FontWeight.normal)),
      ),
    );
  }
}

class RegistrationCell extends StatelessWidget {
  final String title;
  final bool isHeader;

  const RegistrationCell({
    Key? key,
    required this.title,
    this.isHeader = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: isHeader ? Alignment.center : Alignment.center,
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Text(title,
            style: TextStyle(
                fontWeight: isHeader ? FontWeight.bold : FontWeight.bold,
                color: isHeader ? Colors.blue[900] : Colors.black,
                fontSize: isHeader ? 12 : 10),
            textAlign: TextAlign.center),
      ),
    );
  }
}

class ProductGrid extends StatelessWidget {
  const ProductGrid({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.extent(
        maxCrossAxisExtent: 500.0,
        childAspectRatio: 2,
        children: List.generate(6, (i) => Product(index: i)));
  }
}

class Listnames extends StatefulWidget {
  @override
  _ListnamesState createState() => _ListnamesState();
}

class _ListnamesState extends State<Listnames> {
  final ScrollController _firstController = ScrollController();
  List<User> users = [
    User(name: "Hari Prasad Chaudhary", address: "Kathmandu, Nepal"),
    User(name: "David Mars", address: "Bangalore, India"),
    User(name: "Aurn Thapa", address: "Canada"),
    User(name: "John Bal", address: "United States of America")
  ];
  @override
  Widget build(BuildContext context) {
    return Column(
      children: Employee.getEmployees().map((userone) {
        return Card(
          shadowColor: Colors.green[900],
          color: Colors.green[400],
          child: Container(
            margin: const EdgeInsets.all(5),
            padding: const EdgeInsets.all(5),
            color: Colors.green[100],
            child: Column(
              children: [
                ListTile(
                  title: Text("รหัสประจำตัว: ${userone.password}"),
                  subtitle: Text("ชื่อ: ${userone.name}"),
                ),
                Row(
                  children: [
                    Text(" ลำดับ: ${userone.number} "),
                    Text(" สถานภาพ: ${userone.role} "),
                    Text(" กลุ่ม: ${userone.hourlyRate} "),
                  ],
                ),
              ],
            ),
          ),
        );
      }).toList(),
    );
  }
}

class User {
  String name, address;
  User({required this.name, required this.address});
}
