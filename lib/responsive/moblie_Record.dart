import 'package:flutter/material.dart';

class Record extends StatelessWidget {
  const Record({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          alignment: Alignment.center,
          child: Image.network(
            'lib/imags/reg.png',
            width: double.infinity,
            fit: BoxFit.cover,
          ),
        ),
        const SizedBox(
          height: 5,
        ),
        const Text(
          'ระเบียนประวัติ',
          style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: Colors.deepOrange),
        ),
        SizedBox(
          // width: 300,
          height: 500,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              // width: 300,
              // height: 100,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(8)),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    color: const Color.fromARGB(255, 128, 115, 165),
                    child: Table(children: const [
                      TableRow(children: [
                        TableCell(
                          child: SizedBox(
                            height: 20,
                            child: Text(
                              ' ข้อมูลด้านการศึกษา',
                              style: TextStyle(
                                  fontSize: 14.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ]),
                    ]),
                  ),
                  Table(
                    children: [
                      TableRow(children: [
                        Container(
                          child: Table(
                            columnWidths: const {
                              0: FlexColumnWidth(3),
                              1: FlexColumnWidth(10),
                            },
                            defaultColumnWidth: const FixedColumnWidth(70.0),
                            border: TableBorder.all(
                                color: Colors.white,
                                style: BorderStyle.solid,
                                width: 1),
                            children: [
                              TableRow(children: [
                                Container(
                                  height: 15,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[50],
                                  child: Column(children: const [
                                    Text(
                                      ' รหัสประจำตัว:',
                                      style: TextStyle(
                                          fontSize: 8.0, color: Colors.black),
                                    ),
                                  ]),
                                ),
                                Container(
                                  height: 15,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[100],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 2,
                                    ),
                                    Text(' 63160005',
                                        style: TextStyle(
                                            fontSize: 8.0, color: Colors.black))
                                  ]),
                                ),
                              ]),
                              TableRow(children: [
                                Container(
                                  height: 15,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[50],
                                  child: Column(children: const [
                                    Text(
                                      ' เลขที่บัตรประชาชน:',
                                      style: TextStyle(
                                          fontSize: 8.0, color: Colors.black),
                                    ),
                                  ]),
                                ),
                                Container(
                                  height: 15,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[100],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 2,
                                    ),
                                    Text(' 123456788813',
                                        style: TextStyle(
                                            fontSize: 8.0, color: Colors.black))
                                  ]),
                                ),
                              ]),
                              TableRow(children: [
                                Container(
                                  height: 15,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[50],
                                  child: Column(children: const [
                                    Text(
                                      ' ชื่อ:',
                                      style: TextStyle(
                                          fontSize: 8.0, color: Colors.black),
                                    ),
                                  ]),
                                ),
                                Container(
                                  height: 15,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[100],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 2,
                                    ),
                                    Text(' นายทักษ์ติโชค อนุมอญ',
                                        style: TextStyle(
                                            fontSize: 8.0, color: Colors.black))
                                  ]),
                                ),
                              ]),
                              TableRow(children: [
                                Container(
                                  height: 15,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[50],
                                  child: Column(children: const [
                                    Text(
                                      ' ชื่ออังกฤษ:',
                                      style: TextStyle(
                                          fontSize: 8.0, color: Colors.black),
                                    ),
                                  ]),
                                ),
                                Container(
                                  height: 15,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[100],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 2,
                                    ),
                                    Text(' MR. TAKTICHOKE ANUMON',
                                        style: TextStyle(
                                            fontSize: 8.0, color: Colors.black))
                                  ]),
                                ),
                              ]),
                              TableRow(children: [
                                Container(
                                  height: 15,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[50],
                                  child: Column(children: const [
                                    Text(
                                      ' คณะ:',
                                      style: TextStyle(
                                          fontSize: 8.0, color: Colors.black),
                                    ),
                                  ]),
                                ),
                                Container(
                                  height: 15,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[100],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 2,
                                    ),
                                    Text(' คณะวิทยาการสารสนเทศ',
                                        style: TextStyle(
                                            fontSize: 8.0, color: Colors.black))
                                  ]),
                                ),
                              ]),
                              TableRow(children: [
                                Container(
                                  height: 15,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[50],
                                  child: Column(children: const [
                                    Text(
                                      ' วิทยาเขต:',
                                      style: TextStyle(
                                          fontSize: 8.0, color: Colors.black),
                                    ),
                                  ]),
                                ),
                                Container(
                                  height: 15,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[100],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 2,
                                    ),
                                    Text(' บางแสน',
                                        style: TextStyle(
                                            fontSize: 8.0, color: Colors.black))
                                  ]),
                                ),
                              ]),
                              TableRow(children: [
                                Container(
                                  height: 15,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[50],
                                  child: Column(children: const [
                                    Text(
                                      ' หลักสูตร:',
                                      style: TextStyle(
                                          fontSize: 8.0, color: Colors.black),
                                    ),
                                  ]),
                                ),
                                Container(
                                  height: 15,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[100],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 2,
                                    ),
                                    Text(
                                        ' 2115020 วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 58 - ป.ตรี 4 ปี ปกติ ',
                                        style: TextStyle(
                                            fontSize: 8.0, color: Colors.black))
                                  ]),
                                ),
                              ]),
                              TableRow(children: [
                                Container(
                                  height: 15,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[50],
                                  child: Column(children: const [
                                    Text(
                                      ' วิชาโท:',
                                      style: TextStyle(
                                          fontSize: 8.0, color: Colors.black),
                                    ),
                                  ]),
                                ),
                                Container(
                                  height: 15,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[100],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 2,
                                    ),
                                    Text(' -',
                                        style: TextStyle(
                                            fontSize: 8.0, color: Colors.black))
                                  ]),
                                ),
                              ]),
                              TableRow(children: [
                                Container(
                                  height: 15,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[50],
                                  child: Column(children: const [
                                    Text(
                                      ' ระดับการศึกษา:',
                                      style: TextStyle(
                                          fontSize: 8.0, color: Colors.black),
                                    ),
                                  ]),
                                ),
                                Container(
                                  height: 15,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[100],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 2,
                                    ),
                                    Text(' ปริญญาตรี',
                                        style: TextStyle(
                                            fontSize: 8.0, color: Colors.black))
                                  ]),
                                ),
                              ]),
                              TableRow(children: [
                                Container(
                                  height: 15,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[50],
                                  child: Column(children: const [
                                    Text(
                                      ' ชื่อปริญญา:',
                                      style: TextStyle(
                                          fontSize: 8.0, color: Colors.black),
                                    ),
                                  ]),
                                ),
                                Container(
                                  height: 15,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[100],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 2,
                                    ),
                                    Text(
                                        ' วิทยาศาสตรบัณฑิต วท.บ. ปรับปรุง 59-ป.ตรี 4 ปี ปกติ',
                                        style: TextStyle(
                                            fontSize: 8.0, color: Colors.black))
                                  ]),
                                ),
                              ]),
                              TableRow(children: [
                                Container(
                                  height: 25,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[50],
                                  child: Column(children: const [
                                    Text(
                                      ' ปีการศึกษาที่เข้า:',
                                      style: TextStyle(
                                          fontSize: 8.0, color: Colors.black),
                                    ),
                                  ]),
                                ),
                                Container(
                                  height: 25,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[100],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 2,
                                    ),
                                    Text(
                                        ' 2563 / 1' + '\n' + 'วันที่ 5/2/2563 ',
                                        style: TextStyle(
                                            fontSize: 8.0, color: Colors.black))
                                  ]),
                                ),
                              ]),
                              TableRow(children: [
                                Container(
                                  height: 15,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[50],
                                  child: Column(children: const [
                                    Text(
                                      ' สถานภาพ:',
                                      style: TextStyle(
                                          fontSize: 8.0, color: Colors.black),
                                    ),
                                  ]),
                                ),
                                Container(
                                  height: 15,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[100],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 2,
                                    ),
                                    Text(' ',
                                        style: TextStyle(
                                            fontSize: 8.0, color: Colors.black))
                                  ]),
                                ),
                              ]),
                              TableRow(children: [
                                Container(
                                  height: 25,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[50],
                                  child: Column(children: const [
                                    Text(
                                      ' วิธีรับเข้า:',
                                      style: TextStyle(
                                          fontSize: 8.0, color: Colors.black),
                                    ),
                                  ]),
                                ),
                                Container(
                                  height: 25,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[100],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 2,
                                    ),
                                    Text(' ม.6' + '\n' + ' 2.61',
                                        style: TextStyle(
                                            fontSize: 8.0, color: Colors.black))
                                  ]),
                                ),
                              ]),
                              TableRow(children: [
                                Container(
                                  height: 25,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[50],
                                  child: Column(children: const [
                                    Text(
                                      ' จบการศึกษาจาก:',
                                      style: TextStyle(
                                          fontSize: 8.0, color: Colors.black),
                                    ),
                                  ]),
                                ),
                                Container(
                                  height: 25,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[100],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 2,
                                    ),
                                    Text(' พานทองสภาชนูปถัมภ์',
                                        style: TextStyle(
                                            fontSize: 8.0, color: Colors.black))
                                  ]),
                                ),
                              ]),
                              TableRow(children: [
                                Container(
                                  height: 15,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[50],
                                  child: Column(children: const [
                                    Text(
                                      ' อ. ที่ปรึกษา:',
                                      style: TextStyle(
                                          fontSize: 8.0, color: Colors.black),
                                    ),
                                  ]),
                                ),
                                Container(
                                  height: 15,
                                  alignment: Alignment.centerLeft,
                                  color: Colors.deepPurple[100],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 2,
                                    ),
                                    Text(
                                        ' ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม ',
                                        style: TextStyle(
                                            fontSize: 8.0, color: Colors.black))
                                  ]),
                                ),
                              ]),
                            ],
                          ),
                        ),
                      ]),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Card(
                    child: Container(
                      decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(0)),
                      ),
                      height: 150,
                      //   // width: 500,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Row(
                            children: [
                              SizedBox(
                                width: 350,
                                child: Image.network('lib/imags/horz_1.gif'),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                  ' 63160005 : นายทักษ์ติโชค อนุมอญ : คณะวิทยาการสารสนเทศ',
                                  style: TextStyle(
                                      fontSize: 10,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.indigo[900])),
                            ],
                          ),
                          Row(
                            children: const [
                              Text(
                                  ' หลักสูตร: 2115020: วท.บ. (วิทยาการคอมพิวเตอร์)',
                                  style: TextStyle(
                                      fontSize: 10,
                                      fontWeight: FontWeight.bold)),
                            ],
                          ),
                          Row(
                            children: const [
                              Text(
                                  ' ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ : วิชาโท: 0: -',
                                  style: TextStyle(
                                      fontSize: 10,
                                      fontWeight: FontWeight.bold)),
                            ],
                          ),
                          Row(
                            children: const [
                              Text(
                                ' สถานภาพ: กำลังศึกษา',
                                style: TextStyle(
                                    fontSize: 10, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          Row(
                            children: const [
                              Text(
                                ' อ.ที่ปรึกษา ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม',
                                style: TextStyle(
                                    fontSize: 10, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              width: 150,
              child: FloatingActionButton.extended(
                backgroundColor: Colors.grey[800],
                onPressed: () {},
                icon: const Icon(Icons.edit, size: 14),
                label: const Text(
                  "แก้ไขประวัตินิสิต",
                  style: TextStyle(fontSize: 14),
                ),
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            SizedBox(
              child: FloatingActionButton.extended(
                backgroundColor: Colors.grey[800],
                onPressed: () {},
                icon: const Icon(Icons.print, size: 14),
                label: const Text(
                  "พิมพ์ประวัตินิสิต",
                  style: TextStyle(fontSize: 14),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class Recordtable extends StatelessWidget {
  const Recordtable({super.key});

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return Column(
      children: <Widget>[
        Container(
          alignment: Alignment.center,
          child: Image.network(
            'lib/imags/reg.png',
            width: double.infinity,
            fit: BoxFit.cover,
          ),
        ),
        const SizedBox(
          height: 5,
        ),
        const Text(
          'ระเบียนประวัติ',
          style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: Colors.deepOrange),
        ),
        Expanded(
          child: ListView(
            children: [
              SizedBox(
                // width: 300,
                height: 700,
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: screenWidth * 0.1,
                    vertical: screenHeight * 0.1,
                  ),
                  child: Container(
                    // width: 300,
                    // height: 100,
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          color: const Color.fromARGB(255, 128, 115, 165),
                          child: Table(children: const [
                            TableRow(children: [
                              TableCell(
                                child: SizedBox(
                                  height: 20,
                                  child: Text(
                                    ' ข้อมูลด้านการศึกษา',
                                    style: TextStyle(
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            ]),
                          ]),
                        ),
                        Table(
                          children: [
                            TableRow(children: [
                              Container(
                                child: Table(
                                  columnWidths: const {
                                    0: FlexColumnWidth(3),
                                    1: FlexColumnWidth(10),
                                  },
                                  defaultColumnWidth:
                                      const FixedColumnWidth(70.0),
                                  border: TableBorder.all(
                                      color: Colors.white,
                                      style: BorderStyle.solid,
                                      width: 1),
                                  children: [
                                    TableRow(children: [
                                      Container(
                                        height: 15,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[50],
                                        child: Column(children: const [
                                          Text(
                                            ' รหัสประจำตัว:',
                                            style: TextStyle(
                                                fontSize: 8.0,
                                                color: Colors.black),
                                          ),
                                        ]),
                                      ),
                                      Container(
                                        height: 15,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[100],
                                        child: Column(children: const [
                                          SizedBox(
                                            height: 2,
                                          ),
                                          Text(' 63160005',
                                              style: TextStyle(
                                                  fontSize: 8.0,
                                                  color: Colors.black))
                                        ]),
                                      ),
                                    ]),
                                    TableRow(children: [
                                      Container(
                                        height: 15,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[50],
                                        child: Column(children: const [
                                          Text(
                                            ' เลขที่บัตรประชาชน:',
                                            style: TextStyle(
                                                fontSize: 8.0,
                                                color: Colors.black),
                                          ),
                                        ]),
                                      ),
                                      Container(
                                        height: 15,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[100],
                                        child: Column(children: const [
                                          SizedBox(
                                            height: 2,
                                          ),
                                          Text(' 123456788813',
                                              style: TextStyle(
                                                  fontSize: 8.0,
                                                  color: Colors.black))
                                        ]),
                                      ),
                                    ]),
                                    TableRow(children: [
                                      Container(
                                        height: 15,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[50],
                                        child: Column(children: const [
                                          Text(
                                            ' ชื่อ:',
                                            style: TextStyle(
                                                fontSize: 8.0,
                                                color: Colors.black),
                                          ),
                                        ]),
                                      ),
                                      Container(
                                        height: 15,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[100],
                                        child: Column(children: const [
                                          SizedBox(
                                            height: 2,
                                          ),
                                          Text(' นายทักษ์ติโชค อนุมอญ',
                                              style: TextStyle(
                                                  fontSize: 8.0,
                                                  color: Colors.black))
                                        ]),
                                      ),
                                    ]),
                                    TableRow(children: [
                                      Container(
                                        height: 15,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[50],
                                        child: Column(children: const [
                                          Text(
                                            ' ชื่ออังกฤษ:',
                                            style: TextStyle(
                                                fontSize: 8.0,
                                                color: Colors.black),
                                          ),
                                        ]),
                                      ),
                                      Container(
                                        height: 15,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[100],
                                        child: Column(children: const [
                                          SizedBox(
                                            height: 2,
                                          ),
                                          Text(' MR. TAKTICHOKE ANUMON',
                                              style: TextStyle(
                                                  fontSize: 8.0,
                                                  color: Colors.black))
                                        ]),
                                      ),
                                    ]),
                                    TableRow(children: [
                                      Container(
                                        height: 15,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[50],
                                        child: Column(children: const [
                                          Text(
                                            ' คณะ:',
                                            style: TextStyle(
                                                fontSize: 8.0,
                                                color: Colors.black),
                                          ),
                                        ]),
                                      ),
                                      Container(
                                        height: 15,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[100],
                                        child: Column(children: const [
                                          SizedBox(
                                            height: 2,
                                          ),
                                          Text(' คณะวิทยาการสารสนเทศ',
                                              style: TextStyle(
                                                  fontSize: 8.0,
                                                  color: Colors.black))
                                        ]),
                                      ),
                                    ]),
                                    TableRow(children: [
                                      Container(
                                        height: 15,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[50],
                                        child: Column(children: const [
                                          Text(
                                            ' วิทยาเขต:',
                                            style: TextStyle(
                                                fontSize: 8.0,
                                                color: Colors.black),
                                          ),
                                        ]),
                                      ),
                                      Container(
                                        height: 15,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[100],
                                        child: Column(children: const [
                                          SizedBox(
                                            height: 2,
                                          ),
                                          Text(' บางแสน',
                                              style: TextStyle(
                                                  fontSize: 8.0,
                                                  color: Colors.black))
                                        ]),
                                      ),
                                    ]),
                                    TableRow(children: [
                                      Container(
                                        height: 15,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[50],
                                        child: Column(children: const [
                                          Text(
                                            ' หลักสูตร:',
                                            style: TextStyle(
                                                fontSize: 8.0,
                                                color: Colors.black),
                                          ),
                                        ]),
                                      ),
                                      Container(
                                        height: 15,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[100],
                                        child: Column(children: const [
                                          SizedBox(
                                            height: 2,
                                          ),
                                          Text(
                                              ' 2115020 วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 58 - ป.ตรี 4 ปี ปกติ ',
                                              style: TextStyle(
                                                  fontSize: 8.0,
                                                  color: Colors.black))
                                        ]),
                                      ),
                                    ]),
                                    TableRow(children: [
                                      Container(
                                        height: 15,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[50],
                                        child: Column(children: const [
                                          Text(
                                            ' วิชาโท:',
                                            style: TextStyle(
                                                fontSize: 8.0,
                                                color: Colors.black),
                                          ),
                                        ]),
                                      ),
                                      Container(
                                        height: 15,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[100],
                                        child: Column(children: const [
                                          SizedBox(
                                            height: 2,
                                          ),
                                          Text(' -',
                                              style: TextStyle(
                                                  fontSize: 8.0,
                                                  color: Colors.black))
                                        ]),
                                      ),
                                    ]),
                                    TableRow(children: [
                                      Container(
                                        height: 15,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[50],
                                        child: Column(children: const [
                                          Text(
                                            ' ระดับการศึกษา:',
                                            style: TextStyle(
                                                fontSize: 8.0,
                                                color: Colors.black),
                                          ),
                                        ]),
                                      ),
                                      Container(
                                        height: 15,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[100],
                                        child: Column(children: const [
                                          SizedBox(
                                            height: 2,
                                          ),
                                          Text(' ปริญญาตรี',
                                              style: TextStyle(
                                                  fontSize: 8.0,
                                                  color: Colors.black))
                                        ]),
                                      ),
                                    ]),
                                    TableRow(children: [
                                      Container(
                                        height: 15,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[50],
                                        child: Column(children: const [
                                          Text(
                                            ' ชื่อปริญญา:',
                                            style: TextStyle(
                                                fontSize: 8.0,
                                                color: Colors.black),
                                          ),
                                        ]),
                                      ),
                                      Container(
                                        height: 15,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[100],
                                        child: Column(children: const [
                                          SizedBox(
                                            height: 2,
                                          ),
                                          Text(
                                              ' วิทยาศาสตรบัณฑิต วท.บ. ปรับปรุง 59-ป.ตรี 4 ปี ปกติ',
                                              style: TextStyle(
                                                  fontSize: 8.0,
                                                  color: Colors.black))
                                        ]),
                                      ),
                                    ]),
                                    TableRow(children: [
                                      Container(
                                        height: 25,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[50],
                                        child: Column(children: const [
                                          Text(
                                            ' ปีการศึกษาที่เข้า:',
                                            style: TextStyle(
                                                fontSize: 8.0,
                                                color: Colors.black),
                                          ),
                                        ]),
                                      ),
                                      Container(
                                        height: 25,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[100],
                                        child: Column(children: const [
                                          SizedBox(
                                            height: 2,
                                          ),
                                          Text(
                                              ' 2563 / 1' +
                                                  '\n' +
                                                  'วันที่ 5/2/2563 ',
                                              style: TextStyle(
                                                  fontSize: 8.0,
                                                  color: Colors.black))
                                        ]),
                                      ),
                                    ]),
                                    TableRow(children: [
                                      Container(
                                        height: 15,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[50],
                                        child: Column(children: const [
                                          Text(
                                            ' สถานภาพ:',
                                            style: TextStyle(
                                                fontSize: 8.0,
                                                color: Colors.black),
                                          ),
                                        ]),
                                      ),
                                      Container(
                                        height: 15,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[100],
                                        child: Column(children: const [
                                          SizedBox(
                                            height: 2,
                                          ),
                                          Text(' ',
                                              style: TextStyle(
                                                  fontSize: 8.0,
                                                  color: Colors.black))
                                        ]),
                                      ),
                                    ]),
                                    TableRow(children: [
                                      Container(
                                        height: 25,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[50],
                                        child: Column(children: const [
                                          Text(
                                            ' วิธีรับเข้า:',
                                            style: TextStyle(
                                                fontSize: 8.0,
                                                color: Colors.black),
                                          ),
                                        ]),
                                      ),
                                      Container(
                                        height: 25,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[100],
                                        child: Column(children: const [
                                          SizedBox(
                                            height: 2,
                                          ),
                                          Text(' ม.6' + '\n' + ' 2.61',
                                              style: TextStyle(
                                                  fontSize: 8.0,
                                                  color: Colors.black))
                                        ]),
                                      ),
                                    ]),
                                    TableRow(children: [
                                      Container(
                                        height: 25,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[50],
                                        child: Column(children: const [
                                          Text(
                                            ' จบการศึกษาจาก:',
                                            style: TextStyle(
                                                fontSize: 8.0,
                                                color: Colors.black),
                                          ),
                                        ]),
                                      ),
                                      Container(
                                        height: 25,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[100],
                                        child: Column(children: const [
                                          SizedBox(
                                            height: 2,
                                          ),
                                          Text(' พานทองสภาชนูปถัมภ์',
                                              style: TextStyle(
                                                  fontSize: 8.0,
                                                  color: Colors.black))
                                        ]),
                                      ),
                                    ]),
                                    TableRow(children: [
                                      Container(
                                        height: 15,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[50],
                                        child: Column(children: const [
                                          Text(
                                            ' อ. ที่ปรึกษา:',
                                            style: TextStyle(
                                                fontSize: 8.0,
                                                color: Colors.black),
                                          ),
                                        ]),
                                      ),
                                      Container(
                                        height: 15,
                                        alignment: Alignment.centerLeft,
                                        color: Colors.deepPurple[100],
                                        child: Column(children: const [
                                          SizedBox(
                                            height: 2,
                                          ),
                                          Text(
                                              ' ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม ',
                                              style: TextStyle(
                                                  fontSize: 8.0,
                                                  color: Colors.black))
                                        ]),
                                      ),
                                    ]),
                                  ],
                                ),
                              ),
                            ]),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Card(
                          child: Container(
                            decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(0)),
                            ),
                            height: 150,
                            //   // width: 500,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Row(
                                  children: [
                                    SizedBox(
                                      width: 350,
                                      child:
                                          Image.network('lib/imags/horz_1.gif'),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Text(
                                        ' 63160005 : นายทักษ์ติโชค อนุมอญ : คณะวิทยาการสารสนเทศ',
                                        style: TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.indigo[900])),
                                  ],
                                ),
                                Row(
                                  children: const [
                                    Text(
                                        ' หลักสูตร: 2115020: วท.บ. (วิทยาการคอมพิวเตอร์)',
                                        style: TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.bold)),
                                  ],
                                ),
                                Row(
                                  children: const [
                                    Text(
                                        ' ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ : วิชาโท: 0: -',
                                        style: TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.bold)),
                                  ],
                                ),
                                Row(
                                  children: const [
                                    Text(
                                      ' สถานภาพ: กำลังศึกษา',
                                      style: TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: const [
                                    Text(
                                      ' อ.ที่ปรึกษา ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม',
                                      style: TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 150,
                    child: FloatingActionButton.extended(
                      backgroundColor: Colors.grey[800],
                      onPressed: () {},
                      icon: const Icon(Icons.edit, size: 14),
                      label: const Text(
                        "แก้ไขประวัตินิสิต",
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  SizedBox(
                    child: FloatingActionButton.extended(
                      backgroundColor: Colors.grey[800],
                      onPressed: () {},
                      icon: const Icon(Icons.print, size: 14),
                      label: const Text(
                        "พิมพ์ประวัตินิสิต",
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
