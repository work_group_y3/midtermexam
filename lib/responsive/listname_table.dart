import 'package:flutter/material.dart';

import 'listname_cell.dart';
import 'listname.dart';

class ListnameTable extends StatelessWidget {
  const ListnameTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Table(
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      border: TableBorder.all(
          color: Colors.black, style: BorderStyle.solid, width: 1),
      children: <TableRow>[
        TableRow(
          decoration: BoxDecoration(
            color: Colors.green[300],
          ),
          children: const <Widget>[
            RegistrationCell(title: 'ลำดับ', isHeader: true),
            RegistrationCell(title: 'รหัสนิสิต', isHeader: true),
            RegistrationCell(title: 'ชื่อ', isHeader: true),
            RegistrationCell(title: 'สถานภาพ', isHeader: true),
            RegistrationCell(title: 'กลุ่ม', isHeader: true),
          ],
        ),
        ...Employee.getEmployees().map((listname) {
          return TableRow(children: [
            RegistrationCell(title: '${listname.number}'),
            RegistrationCell(title: listname.password),
            RegistrationCell(title: listname.name),
            RegistrationCell(title: listname.role),
            RegistrationCell(title: listname.hourlyRate),
          ]);
        }),
      ],
    );
  }
}

class RegistrationTable extends StatelessWidget {
  const RegistrationTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(0)),
      ),
      // height: 300,
      child: Column(
        children: [
          Container(
            color: Colors.teal.shade700,
            child: Table(children: const [
              TableRow(children: [
                TableCell(
                  child: SizedBox(
                    height: 30,
                    child: Center(
                      child: Text(
                        ' รายวิชาที่ลงทะเบียนทั้งหมด',
                        style: TextStyle(
                            fontSize: 18.0,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
              ]),
            ]),
          ),
          Table(
            columnWidths: const {
              0: FlexColumnWidth(6),
              1: FlexColumnWidth(8),
              2: FlexColumnWidth(7),
              3: FlexColumnWidth(5),
              4: FlexColumnWidth(3),
              5: FlexColumnWidth(3),
            },
            defaultVerticalAlignment: TableCellVerticalAlignment.middle,
            border: TableBorder.all(
                color: Colors.teal.shade700,
                style: BorderStyle.solid,
                width: 0),
            children: <TableRow>[
              TableRow(
                decoration: BoxDecoration(
                  color: Colors.grey[300],
                ),
                children: const <Widget>[
                  RegistrationCell(
                    title: 'รหัสวิชา',
                    isHeader: true,
                  ),
                  RegistrationCell(title: ' ชื่อรายวิชา', isHeader: true),
                  RegistrationCell(title: 'แบบการศึกษา', isHeader: true),
                  RegistrationCell(title: 'หน่วยกิต', isHeader: true),
                  RegistrationCell(title: 'กลุ่ม', isHeader: true),
                  RegistrationCell(title: 'เกรด', isHeader: true),
                ],
              ),
              ...Registration.getEmployees().map((listname) {
                return TableRow(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                    ),
                    children: [
                      RegistrationCell(title: listname.password),
                      RegistrationCell(title: listname.name),
                      RegistrationCell(title: listname.role),
                      RegistrationCell(title: listname.hourlyRate),
                      RegistrationCell(title: listname.team),
                      RegistrationCell(title: listname.grad),
                    ]);
              }),
            ],
          ),
          Container(
            color: Colors.teal.shade700,
            child: Table(children: const [
              TableRow(children: [
                TableCell(
                  child: SizedBox(
                    height: 30,
                    child: Center(
                      child: Text(
                        '                               จำนวนหน่วยกิตรวม 18',
                        style: TextStyle(
                            fontSize: 18.0,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
              ]),
            ]),
          ),
          const SizedBox(
            height: 20,
          ),
          Center(
            child: Card(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      const Text(' ปีการศึกษา'),
                      TextButton(
                        child: Image.network('lib/imags/pull_left_1.gif'),
                        onPressed: () {/* ... */},
                      ),
                      const Text('2564'),
                      // const SizedBox(width: 8),
                      TextButton(
                        child: Image.network('lib/imags/pull_right_1.gif'),
                        onPressed: () {/* ... */},
                      ),
                      const Text('/ '),
                      const Text(' 1'),
                      SizedBox(
                        width: 2,
                        child: TextButton(
                          child: const Text(
                            '2',
                            style: TextStyle(
                                color: Colors.blueAccent,
                                decoration: TextDecoration.underline),
                          ),
                          onPressed: () {/* ... */},
                        ),
                      ),
                      SizedBox(
                        width: 100,
                        child: TextButton(
                          child: const Text(
                            'ฤดูร้อน',
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.blueAccent,
                              decoration: TextDecoration.underline,
                            ),
                          ),
                          onPressed: () {/* ... */},
                        ),
                      ),
                    ],
                  ),
                  // const SizedBox(
                  //   height: 20,
                  // ),
                  SizedBox(
                    height: 150,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Row(
                          children: [
                            SizedBox(
                              width: 350,
                              child: Image.network('lib/imags/horz_1.gif'),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                                ' 63160005 : นายทักษ์ติโชค อนุมอญ : คณะวิทยาการสารสนเทศ',
                                style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.indigo[900])),
                          ],
                        ),
                        Row(
                          children: const [
                            Text(
                                ' หลักสูตร: 2115020: วท.บ. (วิทยาการคอมพิวเตอร์)',
                                style: TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.bold)),
                          ],
                        ),
                        Row(
                          children: const [
                            Text(
                                ' ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ : วิชาโท: 0: -',
                                style: TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.bold)),
                          ],
                        ),
                        Row(
                          children: const [
                            Text(
                              ' สถานภาพ: กำลังศึกษา',
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        Row(
                          children: const [
                            Text(
                              ' อ.ที่ปรึกษา ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม',
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class Product extends StatelessWidget {
  final int index;
  const Product({Key? key, required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 2,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Card(
          color: Colors.green[200],
          child: Row(
            children: [
              Container(
                  color: Colors.green[500],
                  child: const FlutterLogo(size: 190.0)),
              Flexible(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    PricingDetails(price: index),
                    const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                          'Description: the Flutter logo is a beautiful blue icon.',
                          style: TextStyle(fontSize: 18.0)),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
