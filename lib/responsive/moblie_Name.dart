import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';

import 'listname_cell.dart';
import 'listname_table.dart';

class TableExample extends StatelessWidget {
  const TableExample({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(' รายชื่อ'),
        backgroundColor: Colors.grey[900],
      ),
      backgroundColor: Colors.white,
      // backgroundColor: Color.fromARGB(255, 235, 236, 217),
      body: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            child: Image.network(
              'lib/imags/reg.png',
              width: double.infinity,
              fit: BoxFit.cover,
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          Container(
            child: const Text(
              'รายชื่อ',
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.deepOrange),
            ),
          ),
          Expanded(
            child: ListView(
              children: [
                SizedBox(
                  // width: 300,
                  height: 550,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      // width: 300,
                      // height: 100,
                      decoration: const BoxDecoration(
                        color: Color.fromARGB(255, 235, 236, 217),
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                      ),
                      child: const SafeArea(
                        child: Padding(
                          padding: EdgeInsets.all(8.0),
                          child: ListnameTable(),
                        ),
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Align(
                  alignment: Alignment.center,
                  child: TextButton.icon(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const GridExample()),
                      );
                    },
                    icon: const Icon(
                      Icons.ads_click,
                      size: 18.0,
                      color: Colors.black,
                    ),
                    label: Text('Click เพื่อประวัตินิสิต',
                        style:
                            TextStyle(fontSize: 16, color: Colors.blue[800])),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class GridExample extends StatelessWidget {
  const GridExample({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool _isAlwaysShown = true;

    bool _showTrackOnHover = false;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text(' รายชื่อ'),
        backgroundColor: Colors.grey[900],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Scrollbar(
              thumbVisibility: _isAlwaysShown,
              trackVisibility: _showTrackOnHover,
              hoverThickness: 30.0,
              child: ListView(
                children: [
                  Listnames(),
                ],
              ),
            ),
          ),
          Divider(height: 1),
        ],
      ),
    );
  }
}
