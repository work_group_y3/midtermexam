import 'package:flutter/material.dart';

class Results extends StatelessWidget {
  const Results({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(' ผลการศึกษา'),
        backgroundColor: Colors.grey[900],
      ),
      backgroundColor: const Color.fromARGB(255, 235, 236, 217),
      body: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            child: Image.network(
              'lib/imags/reg.png',
              width: double.infinity,
              fit: BoxFit.cover,
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          Expanded(
            child: ListView(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    // width: 300,
                    // height: 50,
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ),
                    child: Column(
                      // mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          color: Colors.indigo[900],
                          child: Table(children: const [
                            TableRow(children: [
                              TableCell(
                                child: SizedBox(
                                  height: 20,
                                  child: Center(
                                    child: Text(
                                      ' ภาคการศึกษาที่ 1/2563',
                                      style: TextStyle(
                                          fontSize: 14.0,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              ),
                            ]),
                          ]),
                        ),
                        Table(
                          children: [
                            TableRow(children: [
                              Table(
                                columnWidths: const {
                                  0: FlexColumnWidth(2),
                                  1: FlexColumnWidth(5),
                                  2: FlexColumnWidth(2),
                                  3: FlexColumnWidth(1),
                                },
                                defaultColumnWidth:
                                    const FixedColumnWidth(70.0),
                                border: TableBorder.all(
                                    color: Colors.white,
                                    style: BorderStyle.solid,
                                    width: 1),
                                children: [
                                  TableRow(children: [
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[100],
                                      child: Column(children: const [
                                        Text(
                                          ' รหัสวิชา',
                                          style: TextStyle(
                                              fontSize: 12.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[100],
                                      child: Column(children: const [
                                        Text(
                                          ' ชื่อรายวิชา',
                                          style: TextStyle(
                                              fontSize: 12.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[100],
                                      child: Column(children: const [
                                        Text(
                                          ' หน่วยกิต',
                                          style: TextStyle(
                                              fontSize: 12.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[100],
                                      child: Column(children: const [
                                        Text(
                                          ' เกรด',
                                          style: TextStyle(
                                              fontSize: 12.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                  ]),
                                  TableRow(children: [
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' 30910159',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' Marine Ecology and Ecotourism',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' 2',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' B',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                  ]),
                                  TableRow(children: [
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' 40240359',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' Sufficiency Economy and Social',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' 2',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' A',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                  ]),
                                  TableRow(children: [
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' 85111059',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' Exercise for Quality of Life',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' 2',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' A',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                  ]),
                                  TableRow(children: [
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' 88510059',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' Logical Thinking and Problem',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' 2',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' B',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                  ]),
                                  TableRow(children: [
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' 88510159',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' Moving Forward in a Digital ICT',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' 2',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' B',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                  ]),
                                  TableRow(children: [
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' 88510259',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' Discrete Structures',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' 2',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' A',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                  ]),
                                  TableRow(children: [
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' 99910259',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' Collegiate English',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' 3',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                    Container(
                                      height: 20,
                                      alignment: Alignment.center,
                                      color: Colors.deepPurple[50],
                                      child: Column(children: const [
                                        Text(
                                          ' A',
                                          style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ]),
                                    ),
                                  ]),
                                ],
                              ),
                            ]),
                          ],
                        ),
                        Container(
                          color: Colors.grey[300],
                          child: Table(children: [
                            TableRow(children: [
                              TableCell(
                                child: SizedBox(
                                  height: 20,
                                  child: Center(
                                    child: Text(
                                      '',
                                      style: TextStyle(
                                          fontSize: 12.0,
                                          color: Colors.blue[900],
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              ),
                              TableCell(
                                child: SizedBox(
                                  // height: 20,
                                  child: Center(
                                    child: Text(
                                      'ผลการศึกษา',
                                      style: TextStyle(
                                          fontSize: 14.0,
                                          color: Colors.blue[900],
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              ),
                            ]),
                            TableRow(
                              children: [
                                TableCell(
                                  child: SizedBox(
                                    height: 20,
                                    child: Center(
                                      child: Text(
                                        'THIS SEMESTER',
                                        style: TextStyle(
                                            fontSize: 12.0,
                                            color: Colors.blue[900],
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                ),
                                TableCell(
                                  child: SizedBox(
                                    height: 20,
                                    child: Center(
                                      child: Text(
                                        'CUMULATIVE TO THIS SEMESTER',
                                        style: TextStyle(
                                            fontSize: 12.0,
                                            color: Colors.blue[900],
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ]),
                        ),
                        Center(
                          child: Table(
                            children: [
                              TableRow(
                                children: [
                                  TableCell(
                                    child: Table(
                                      columnWidths: const {
                                        0: FlexColumnWidth(2),
                                        1: FlexColumnWidth(2),
                                        2: FlexColumnWidth(2),
                                        3: FlexColumnWidth(2),
                                        4: FlexColumnWidth(2),
                                      },
                                      defaultColumnWidth:
                                          const FixedColumnWidth(70.0),
                                      border: TableBorder.all(
                                          color: Colors.grey.shade300,
                                          style: BorderStyle.solid,
                                          width: 1),
                                      children: [
                                        TableRow(children: [
                                          Container(
                                            height: 30,
                                            color: Colors.teal.shade50,
                                            child: Column(children: const [
                                              Text(
                                                'C.Register' +
                                                    '\n' +
                                                    '        17',
                                                style: TextStyle(
                                                    fontSize: 8.0,
                                                    color: Colors.black,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ]),
                                          ),
                                          Container(
                                            height: 30,
                                            color: Colors.teal.shade50,
                                            child: Column(children: const [
                                              Text(
                                                ' C.Earn' + '\n' + '     17',
                                                style: TextStyle(
                                                    fontSize: 8.0,
                                                    color: Colors.black,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ]),
                                          ),
                                          Container(
                                            height: 30,
                                            alignment: Alignment.center,
                                            color: Colors.teal.shade50,
                                            child: Column(children: const [
                                              Text(
                                                ' CA' + '\n' + ' 17',
                                                style: TextStyle(
                                                    fontSize: 8.0,
                                                    color: Colors.black,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ]),
                                          ),
                                          Container(
                                            height: 30,
                                            alignment: Alignment.center,
                                            color: Colors.teal.shade50,
                                            child: Column(children: const [
                                              Text(
                                                ' GP' + '\n' + ' 59',
                                                style: TextStyle(
                                                    fontSize: 8.0,
                                                    color: Colors.black,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ]),
                                          ),
                                          Container(
                                            height: 30,
                                            alignment: Alignment.center,
                                            color: Colors.teal.shade50,
                                            child: Column(children: const [
                                              Text(
                                                ' GPA' + '\n' + ' 3.74',
                                                style: TextStyle(
                                                    fontSize: 8.0,
                                                    color: Colors.black,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ]),
                                          ),
                                        ]),
                                      ],
                                    ),
                                  ),
                                  TableCell(
                                    child: Table(
                                      columnWidths: const {
                                        0: FlexColumnWidth(2),
                                        1: FlexColumnWidth(2),
                                        2: FlexColumnWidth(2),
                                        3: FlexColumnWidth(2),
                                        4: FlexColumnWidth(2),
                                      },
                                      defaultColumnWidth:
                                          const FixedColumnWidth(70.0),
                                      border: TableBorder.all(
                                          color: Colors.grey.shade300,
                                          style: BorderStyle.solid,
                                          width: 1),
                                      children: [
                                        TableRow(children: [
                                          Container(
                                            height: 30,
                                            // color: Colors.teal.shade50,
                                            child: Column(children: const [
                                              Text(
                                                'C.Register' +
                                                    '\n' +
                                                    '        17',
                                                style: TextStyle(
                                                    fontSize: 8.0,
                                                    color: Colors.black,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ]),
                                          ),
                                          Container(
                                            height: 30,
                                            alignment: Alignment.center,
                                            // color: Colors.teal.shade50,
                                            child: Column(children: const [
                                              Text(
                                                ' C.Earn' + '\n' + '     17',
                                                style: TextStyle(
                                                    fontSize: 8.0,
                                                    color: Colors.black,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ]),
                                          ),
                                          Container(
                                            height: 30,
                                            alignment: Alignment.center,
                                            // color: Colors.teal.shade50,
                                            child: Column(children: const [
                                              Text(
                                                ' CA' + '\n' + ' 17',
                                                style: TextStyle(
                                                    fontSize: 8.0,
                                                    color: Colors.black,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ]),
                                          ),
                                          Container(
                                            height: 30,
                                            alignment: Alignment.center,
                                            // color: Colors.teal.shade50,
                                            child: Column(children: const [
                                              Text(
                                                ' GP' + '\n' + ' 59',
                                                style: TextStyle(
                                                    fontSize: 8.0,
                                                    color: Colors.black,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ]),
                                          ),
                                          Container(
                                            height: 30,
                                            alignment: Alignment.center,
                                            // color: Colors.teal.shade50,
                                            child: Column(children: const [
                                              Text(
                                                ' GPA' + '\n' + ' 3.74',
                                                style: TextStyle(
                                                    fontSize: 8.0,
                                                    color: Colors.black,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ]),
                                          ),
                                        ]),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 200,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              const SizedBox(
                                height: 20,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  const Text(' แสดงข้อมูล',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                  SizedBox(
                                    width: 60,
                                    child: TextButton(
                                      child: const Text(
                                        'ทั้งหมด',
                                        style: TextStyle(
                                            fontSize: 12,
                                            color: Colors.blueAccent,
                                            decoration:
                                                TextDecoration.underline),
                                      ),
                                      onPressed: () {/* ... */},
                                    ),
                                  ),
                                  SizedBox(
                                    width: 30,
                                    child: TextButton(
                                      child: Image.network(
                                          'lib/imags/pull_right_1.gif'),
                                      onPressed: () {/* ... */},
                                    ),
                                  ),
                                  const Text(' | '),
                                  SizedBox(
                                    width: 50,
                                    child: TextButton(
                                      child: const Text(
                                        '2563',
                                        style: TextStyle(
                                            fontSize: 12,
                                            color: Colors.blueAccent,
                                            decoration:
                                                TextDecoration.underline),
                                      ),
                                      onPressed: () {/* ... */},
                                    ),
                                  ),
                                  SizedBox(
                                    width: 2,
                                    child: TextButton(
                                      child: const Text(
                                        '1',
                                        style: TextStyle(
                                            fontSize: 12,
                                            color: Colors.blueAccent,
                                            decoration:
                                                TextDecoration.underline),
                                      ),
                                      onPressed: () {/* ... */},
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  SizedBox(
                                    width: 2,
                                    child: TextButton(
                                      child: const Text(
                                        '2',
                                        style: TextStyle(
                                            fontSize: 12,
                                            color: Colors.blueAccent,
                                            decoration:
                                                TextDecoration.underline),
                                      ),
                                      onPressed: () {/* ... */},
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  SizedBox(
                                    width: 350,
                                    child:
                                        Image.network('lib/imags/horz_1.gif'),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  Text(
                                      ' 63160005 : นายทักษ์ติโชค อนุมอญ : คณะวิทยาการสารสนเทศ',
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.indigo[900])),
                                ],
                              ),
                              Row(
                                children: const [
                                  Text(
                                      ' หลักสูตร: 2115020: วท.บ. (วิทยาการคอมพิวเตอร์)',
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold)),
                                ],
                              ),
                              Row(
                                children: const [
                                  Text(
                                      ' ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ : วิชาโท: 0: -',
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold)),
                                ],
                              ),
                              Row(
                                children: const [
                                  Text(
                                    ' สถานภาพ: กำลังศึกษา',
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              Row(
                                children: const [
                                  Text(
                                    ' อ.ที่ปรึกษา ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม',
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
