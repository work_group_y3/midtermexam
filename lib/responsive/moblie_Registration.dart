import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';

import 'listname_table.dart';
import 'moblie_Home.dart';
import 'moblie_Record.dart';
import 'moblie_RegistrationTable.dart';
import 'moblie_Tableclass.dart';

class TableRegistration extends StatefulWidget {
  const TableRegistration({super.key});

  @override
  State<TableRegistration> createState() => _TableRegistrationState();
}

class _TableRegistrationState extends State<TableRegistration> {
  int currentIndex = 1;
  List widfgetOptions = [
    RegistrationEdit(),
    RegistrationTables(),
    RegistrationHistory(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(' ผลลงทะเบียน'),
        backgroundColor: Colors.grey[900],
      ),
      backgroundColor: Color.fromARGB(255, 235, 236, 217),
      body: widfgetOptions[currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.score), label: 'คะแนน'),
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'หน้าหลัก'),
          BottomNavigationBarItem(
              icon: Icon(Icons.history), label: 'ประวัติทำรายการ'),
        ],
        currentIndex: currentIndex,
        onTap: (index) => setState(() => currentIndex = index),
      ),
    );
  }
}

class TableRegistrationtable extends StatefulWidget {
  const TableRegistrationtable({super.key});

  @override
  State<TableRegistrationtable> createState() => _TableRegistrationStatetable();
}

class _TableRegistrationStatetable extends State<TableRegistrationtable> {
  int currentIndex = 1;
  List widfgetOptions = [
    RegistrationEdit(),
    RegistrationTablestable(),
    RegistrationHistory(),
  ];
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text(' ผลลงทะเบียน'),
        backgroundColor: Colors.grey[900],
      ),
      backgroundColor: Color.fromARGB(255, 235, 236, 217),
      body: widfgetOptions[currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.score), label: 'คะแนน'),
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'หน้าหลัก'),
          BottomNavigationBarItem(
              icon: Icon(Icons.history), label: 'ประวัติทำรายการ'),
        ],
        currentIndex: currentIndex,
        onTap: (index) => setState(() => currentIndex = index),
      ),
    );
  }
}
