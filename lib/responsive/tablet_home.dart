import 'package:flutter/material.dart';
import '../constants.dart';
import '../main.dart';
import '../util/utilities.dart';
import 'mobile_Approval .dart';
import 'mobile_Comment.dart';
import 'mobile_Paystude.dart';
import 'mobile_Petition.dart';
import 'moblie_Home.dart';
import 'moblie_Login.dart';
import 'moblie_Name.dart';
import 'moblie_Record.dart';
import 'moblie_Registration.dart';
import 'moblie_Results.dart';
import 'moblie_Tableclass.dart';

class TabletScaffold extends StatefulWidget {
  const TabletScaffold({Key? key}) : super(key: key);

  @override
  State<TabletScaffold> createState() => _TabletScaffoldState();
}

class _TabletScaffoldState extends State<TabletScaffold> {
  int currentIndex = 1;
  List widfgetOptions = [
    const Tableclasstable(),
    const Hometable(),
    const Recordtable(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: myAppBar,
      backgroundColor: myDefaultBackground,
      drawer: Drawer(
        backgroundColor: Colors.grey[300],
        child: Scrollbar(
          child: ListView(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  DrawerHeader(
                    child: Image.network(
                      'lib/imags/63160005.jpg',
                      height: 150,
                      width: double.infinity,
                      // fit: BoxFit.contain,
                    ),
                  ),
                  Text('นาย ทักษ์ติโชค อนุมอญ', style: TextStyle(fontSize: 24)),
                  SizedBox(
                    height: 30,
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: TextButton.icon(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const HomePage()),
                        );
                      },
                      icon: const Icon(
                        Icons.home,
                        size: 18.0,
                        color: Colors.black,
                      ),
                      label: const Text('หน้าหลัก',
                          style: TextStyle(fontSize: 16, color: Colors.black)),
                    ),
                  ),
                  // SizedBox(
                  //   height: 15,
                  // ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: TextButton.icon(
                      onPressed: () => showDialog<String>(
                        context: context,
                        builder: (BuildContext context) => AlertDialog(
                          title: const Text('reg.buu.ac.th บอกว่า'),
                          content: const Text(
                              'นิสิตทำการลงทะเบียนแล้วโปรดตรวจสอบรายวิชาที่เมนูผลการลงทะเบียน',
                              style: TextStyle(fontSize: 14)),
                          actions: <Widget>[
                            TextButton(
                              onPressed: () => Navigator.pop(context, 'ตกลง'),
                              child: const Text('ตกลง'),
                            ),
                          ],
                        ),
                      ),
                      icon: const Icon(
                        null,
                        size: 18.0,
                        color: Colors.black,
                      ),
                      label: const Text('ลงทะเบียน',
                          style: TextStyle(fontSize: 16, color: Colors.black)),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: TextButton.icon(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  const TableRegistrationtable()),
                        );
                      },
                      icon: const Icon(
                        null,
                        size: 18.0,
                        color: Colors.black,
                      ),
                      label: const Text('ผลการลงทะเบียน',
                          style: TextStyle(fontSize: 16, color: Colors.black)),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: TextButton.icon(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const Approval()),
                        );
                      },
                      icon: const Icon(
                        null,
                        size: 18.0,
                        color: Colors.black,
                      ),
                      label: const Text('ผลการอนุมัติเพิ่ม-ลด',
                          style: TextStyle(fontSize: 16, color: Colors.black)),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: TextButton.icon(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const Paystude()),
                        );
                      },
                      icon: const Icon(
                        null,
                        size: 18.0,
                        color: Colors.black,
                      ),
                      label: const Text('ภาระค่าใช้จ่ายทุน',
                          style: TextStyle(fontSize: 16, color: Colors.black)),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: TextButton.icon(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const Results()),
                        );
                      },
                      icon: const Icon(
                        null,
                        size: 18.0,
                        color: Colors.black,
                      ),
                      label: const Text('ผลการศึกษา',
                          style: TextStyle(fontSize: 16, color: Colors.black)),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: TextButton.icon(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const Petition()),
                        );
                      },
                      icon: const Icon(
                        null,
                        size: 18.0,
                        color: Colors.black,
                      ),
                      label: const Text('ยื่นคำร้อง',
                          style: TextStyle(fontSize: 16, color: Colors.black)),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: TextButton.icon(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const Comment()),
                        );
                      },
                      icon: const Icon(
                        null,
                        size: 18.0,
                        color: Colors.black,
                      ),
                      label: const Text('เสนอความคิดเห็น',
                          style: TextStyle(fontSize: 16, color: Colors.black)),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: TextButton.icon(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const TableExample()),
                        );
                      },
                      icon: const Icon(
                        null,
                        size: 18.0,
                        color: Colors.black,
                      ),
                      label: const Text('รายชื่อนิสิต',
                          style: TextStyle(fontSize: 16, color: Colors.black)),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: TextButton.icon(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const Login()),
                        );
                      },
                      icon: const Icon(
                        Icons.logout,
                        size: 18.0,
                        color: Colors.black,
                      ),
                      label: const Text('ออกจากระบบ',
                          style: TextStyle(fontSize: 16, color: Colors.black)),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      body: widfgetOptions[currentIndex],
      bottomNavigationBar: SizedBox(
        height: 72.0,
        child: BottomNavigationBar(
          // selectedFontSize: 25,
          unselectedFontSize: 12,
          items: const [
            BottomNavigationBarItem(
                icon: Icon(Icons.table_chart, size: 16.0),
                label: 'ตารางเรียนนิสิต'),
            BottomNavigationBarItem(
                icon: Icon(Icons.home, size: 16.0), label: 'หน้าหลัก'),
            BottomNavigationBarItem(
                icon: Icon(Icons.assignment_ind, size: 16.0),
                label: 'ประวัตินิสิต'),
          ],
          currentIndex: currentIndex,
          onTap: (index) => setState(() => currentIndex = index),
        ),
      ),
    );
  }
}
