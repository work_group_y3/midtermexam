import 'package:flutter/material.dart';

class Tableclass extends StatelessWidget {
  const Tableclass({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          alignment: Alignment.center,
          child: Image.network(
            'lib/imags/reg.png',
            width: double.infinity,
            fit: BoxFit.cover,
          ),
        ),
        const SizedBox(
          height: 5,
        ),
        const Text(
          'ตารางเรียน/สอบของรายวิชาที่ลงทะเบียนไว้แล้ว',
          style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: Colors.deepOrange),
        ),
        AspectRatio(
          aspectRatio: 1,
          child: SizedBox(
            width: double.infinity,
            child: GridView.count(
              primary: false,
              padding: const EdgeInsets.all(20),
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              crossAxisCount: 1,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Container(
                    width: 300,
                    height: 100,
                    decoration: BoxDecoration(
                      color: Colors.grey[300],
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        const Text(
                          "ตารางเรียน",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        Container(
                          margin: EdgeInsets.all(20),
                          child: Table(
                            columnWidths: const {
                              0: FlexColumnWidth(4),
                              1: FlexColumnWidth(8),
                              2: FlexColumnWidth(8),
                              3: FlexColumnWidth(8),
                            },
                            defaultColumnWidth: const FixedColumnWidth(70.0),
                            border: TableBorder.all(
                                color: Colors.white,
                                style: BorderStyle.solid,
                                width: 1),
                            children: [
                              TableRow(children: [
                                Container(
                                  height: 20,
                                  color: Colors.grey[850],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      'Day/Time',
                                      style: TextStyle(
                                          fontSize: 8.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white70),
                                    ),
                                  ]),
                                ),
                                Container(
                                  height: 20,
                                  color: Colors.grey[850],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text('10:00-12:00',
                                        style: TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                                Container(
                                  height: 20,
                                  color: Colors.grey[850],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text('13:00-16:00	',
                                        style: TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                                Container(
                                  height: 20,
                                  color: Colors.grey[850],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text('17:00-19:00	',
                                        style: TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                              ]),
                              TableRow(children: [
                                Container(
                                  height: 40,
                                  color: Colors.grey,
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(' จันทร์',
                                        style: TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                                Container(
                                  height: 40,
                                  color: Colors.indigo[200],
                                  child: Column(children: const [
                                    Text('88624559-59',
                                        style: TextStyle(fontSize: 10.0)),
                                    Text('(3) 2, IF-4M210',
                                        style: TextStyle(fontSize: 10.0)),
                                    Text('IF',
                                        style: TextStyle(fontSize: 10.0)),
                                  ]),
                                ),
                                Column(children: const [Text('')]),
                                Column(children: const [Text('')]),
                              ]),
                              TableRow(children: [
                                Container(
                                  height: 40,
                                  color: Colors.grey,
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(' อังคาร',
                                        style: TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                                Column(children: const [Text('')]),
                                Column(children: const [Text('')]),
                                Container(
                                  height: 40,
                                  color: Colors.indigo[200],
                                  child: Column(children: const [
                                    Text('88624459-59',
                                        style: TextStyle(fontSize: 10.0)),
                                    Text('(3) 2, IF-3C01',
                                        style: TextStyle(fontSize: 10.0)),
                                    Text('IF',
                                        style: TextStyle(fontSize: 10.0)),
                                  ]),
                                ),
                              ]),
                              TableRow(children: [
                                Container(
                                  height: 40,
                                  color: Colors.grey,
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(' พุธ',
                                        style: TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                                Column(children: const [Text('')]),
                                Container(
                                  height: 40,
                                  color: Colors.indigo[200],
                                  child: Column(children: const [
                                    Text('88624459-59',
                                        style: TextStyle(fontSize: 10.0)),
                                    Text('(3) 2, IF-3C01',
                                        style: TextStyle(fontSize: 10.0)),
                                    Text('IF',
                                        style: TextStyle(fontSize: 10.0)),
                                  ]),
                                ),
                                Column(children: [Text('')]),
                              ]),
                              TableRow(children: [
                                Container(
                                  height: 40,
                                  color: Colors.grey,
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(' พฤหัสบดี',
                                        style: TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                                Container(
                                  height: 40,
                                  color: Colors.indigo[200],
                                  child: Column(children: const [
                                    Text('88624559-59',
                                        style: TextStyle(fontSize: 10.0)),
                                    Text('(3) 2, IF-4M210',
                                        style: TextStyle(fontSize: 10.0)),
                                    Text('IF',
                                        style: TextStyle(fontSize: 10.0)),
                                  ]),
                                ),
                                Container(
                                  height: 40,
                                  color: Colors.indigo[200],
                                  child: Column(children: const [
                                    Text('88624559-59',
                                        style: TextStyle(fontSize: 10.0)),
                                    Text('(3) 2, IF-4M210',
                                        style: TextStyle(fontSize: 10.0)),
                                    Text('IF',
                                        style: TextStyle(fontSize: 10.0)),
                                  ]),
                                ),
                                Column(children: const [Text('')]),
                              ]),
                              TableRow(children: [
                                Container(
                                  height: 40,
                                  color: Colors.grey,
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(' ศุกร์',
                                        style: TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                                Column(children: const [Text('')]),
                                Column(children: const [Text('')]),
                                Container(
                                  height: 40,
                                  color: Colors.indigo[200],
                                  child: Column(children: const [
                                    Text('88624459-59',
                                        style: TextStyle(fontSize: 10.0)),
                                    Text('(3) 2, IF-3C01',
                                        style: TextStyle(fontSize: 10.0)),
                                    Text('IF',
                                        style: TextStyle(fontSize: 10.0)),
                                  ]),
                                ),
                              ]),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Container(
                    width: 300,
                    height: 100,
                    decoration: BoxDecoration(
                      color: Colors.grey[300],
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        const Text(
                          "ตารางสอบ",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        Container(
                          color: Colors.white60,
                          margin: EdgeInsets.all(20),
                          child: Table(
                            columnWidths: const {
                              0: FlexColumnWidth(1),
                              1: FlexColumnWidth(2),
                              2: FlexColumnWidth(1),
                              // 3: FlexColumnWidth(1),
                              // 4: FlexColumnWidth(1),
                            },
                            defaultColumnWidth: FixedColumnWidth(70.0),
                            border: TableBorder.all(
                                color: Colors.white60,
                                style: BorderStyle.solid,
                                width: 1),
                            children: [
                              TableRow(children: [
                                Container(
                                  height: 20,
                                  color: Colors.grey[300],
                                  child: Column(children: [
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      ' รหัสวิชา',
                                      style: TextStyle(
                                          fontSize: 9.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.blue[900]),
                                    ),
                                  ]),
                                ),
                                Container(
                                  height: 20,
                                  color: Colors.grey[300],
                                  child: Column(children: [
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Text(' ชื่อรายวิชา',
                                        style: TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.blue[900]))
                                  ]),
                                ),
                                Container(
                                  height: 20,
                                  color: Colors.grey[300],
                                  child: Column(children: [
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Text(' กลุ่ม',
                                        style: TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.blue[900]))
                                  ]),
                                ),
                                Container(
                                  height: 20,
                                  color: Colors.grey[300],
                                  child: Column(children: [
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Text(' สอบกลางภาค',
                                        style: TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.blue[900]))
                                  ]),
                                ),
                                Container(
                                  height: 20,
                                  color: Colors.grey[300],
                                  child: Column(children: [
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Text(' สอบปลายภาค',
                                        style: TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.blue[900]))
                                  ]),
                                ),
                              ]),
                              TableRow(children: [
                                Container(
                                  // height: 40,
                                  // color: Colors.grey,
                                  child: Column(children: [
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Text(' 88624459-59',
                                        style: TextStyle(
                                            fontSize: 5.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black))
                                  ]),
                                ),
                                Container(
                                  // height: 40,
                                  // color: Colors.indigo[200],
                                  child: Column(children: const [
                                    Text(
                                        ' Object-Oriented Analysis and Design' +
                                            '\n' +
                                            ' การวิเคราะห์และออกแบบเชิงวัตถุ',
                                        style: TextStyle(
                                            fontSize: 5.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black)),
                                  ]),
                                ),
                                Column(children: const [
                                  Text('2',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [
                                  Text(
                                      ' 17 ม.ค. 2566 (C)' +
                                          '\n' +
                                          ' เวลา 17:00-20:00' '\n' +
                                          '       IF-11M280',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [Text('-')]),
                              ]),
                              TableRow(children: [
                                Column(children: const [
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(' 88646259-59',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [
                                  Text(
                                      ' Introduction to Natural Language Processing' +
                                          '\n' +
                                          ' การประมวลผลภาษาธรรมชาติเบื้องต้น',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black)),
                                ]),
                                Column(children: const [
                                  Text('2',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [
                                  Text(
                                      ' 18 ม.ค. 2566 (C)' +
                                          '\n' +
                                          ' เวลา 17:00-20:00' '\n' +
                                          '       IF-11M280',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [Text('-')]),
                              ]),
                              TableRow(children: [
                                Column(children: const [
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(' 88624459-59',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [
                                  Text(
                                      ' Object-Oriented Analysis and Design' +
                                          '\n' +
                                          ' การวิเคราะห์และออกแบบเชิงวัตถุ',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black)),
                                ]),
                                Column(children: const [
                                  Text('2',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [
                                  Text(
                                      ' 17 ม.ค. 2566 (C)' +
                                          '\n' +
                                          ' เวลา 17:00-20:00' '\n' +
                                          '       IF-11M280',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [Text('-')]),
                              ]),
                              TableRow(children: [
                                Column(children: const [
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(' 88634259-59 ',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Container(
                                  // height: 40,
                                  // color: Colors.indigo[200],
                                  child: Column(children: const [
                                    Text(
                                        ' Introduction to Natural Language Processing' +
                                            '\n' +
                                            ' การประมวลผลภาษาธรรมชาติเบื้องต้น',
                                        style: TextStyle(
                                            fontSize: 5.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black)),
                                  ]),
                                ),
                                Column(children: const [
                                  Text('2',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [
                                  Text(
                                      ' 18 ม.ค. 2566 (C)' +
                                          '\n' +
                                          ' เวลา 17:00-20:00' '\n' +
                                          '       IF-11M280',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: [Text('-')]),
                              ]),
                              TableRow(children: [
                                Column(children: const [
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(' 88624459-59',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [
                                  Text(
                                      ' Object-Oriented Analysis and Design' +
                                          '\n' +
                                          ' การวิเคราะห์และออกแบบเชิงวัตถุ',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black)),
                                ]),
                                Column(children: const [
                                  Text('2',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [
                                  Text(
                                      ' 17 ม.ค. 2566 (C)' +
                                          '\n' +
                                          ' เวลา 17:00-20:00' '\n' +
                                          '       IF-11M280',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [Text('-')]),
                              ]),
                              TableRow(children: [
                                Column(children: const [
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(' 88634259-59 ',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [
                                  Text(
                                      ' Introduction to Natural Language Processing' +
                                          '\n' +
                                          ' การประมวลผลภาษาธรรมชาติเบื้องต้น',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black)),
                                ]),
                                Column(children: const [
                                  Text('2',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: [
                                  Text(
                                      ' 18 ม.ค. 2566 (C)' +
                                          '\n' +
                                          ' เวลา 17:00-20:00' '\n' +
                                          '       IF-11M280',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: [Text('-')]),
                              ]),
                              TableRow(children: [
                                Container(
                                  // height: 40,
                                  // color: Colors.grey,
                                  child: Column(children: [
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(' 88624459-59',
                                        style: TextStyle(
                                            fontSize: 5.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black))
                                  ]),
                                ),
                                Container(
                                  // height: 40,
                                  // color: Colors.indigo[200],
                                  child: Column(children: [
                                    Text(
                                        ' Object-Oriented Analysis and Design' +
                                            '\n' +
                                            ' การวิเคราะห์และออกแบบเชิงวัตถุ',
                                        style: TextStyle(
                                            fontSize: 5.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black)),
                                  ]),
                                ),
                                Column(children: [
                                  Text('2',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: [
                                  Text(
                                      ' 17 ม.ค. 2566 (C)' +
                                          '\n' +
                                          ' เวลา 17:00-20:00' '\n' +
                                          '       IF-11M280',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: [Text('-')]),
                              ]),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Expanded(
            child: ListView(
          padding: const EdgeInsets.all(8),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(0)),
                ),
                height: 200,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Row(
                      children: const [
                        Text(' ชื่อ             ทักษ์ติโชค อนุมอญ',
                            style: TextStyle(
                                fontSize: 12, fontWeight: FontWeight.bold)),
                      ],
                    ),
                    Row(
                      children: const [
                        Text(' สถานภาพ  กำลังศึกษา',
                            style: TextStyle(
                                fontSize: 12, fontWeight: FontWeight.bold)),
                      ],
                    ),
                    Row(
                      children: const [
                        Text(' คณะ          คณะวิทยาการสารสนเทศ',
                            style: TextStyle(
                                fontSize: 12, fontWeight: FontWeight.bold)),
                        // Text(
                        //     'หลักสูตร  วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติวิทยาศาสตรบัณฑิต (วท.บ.)'),
                      ],
                    ),
                    Row(
                      children: const [
                        Text(
                          ' หลักสูตร    วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ' +
                              '\n' +
                              '                   วิทยาศาสตรบัณฑิต (วท.บ.)',
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    Row(
                      children: const [
                        Text(
                          ' อ.ที่ปรึกษา ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม',
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.all(20),
                      child: Table(
                        columnWidths: const {
                          0: FlexColumnWidth(1),
                          1: FlexColumnWidth(1),
                        },
                        defaultColumnWidth: FixedColumnWidth(70.0),
                        border: TableBorder.all(
                          color: Colors.black,
                          style: BorderStyle.solid,
                          width: 1,
                        ),
                        children: [
                          TableRow(children: [
                            TableCell(
                              child: SizedBox(
                                height: 15,
                                child: Container(
                                  color: Colors.indigo[900],
                                  child: Column(children: const [
                                    Text('Thesis',
                                        style: TextStyle(
                                            fontSize: 10.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white)),
                                  ]),
                                ),
                              ),
                            ),
                          ]),
                          const TableRow(children: [
                            TableCell(
                              child: SizedBox(
                                height: 30,
                                child: Center(
                                  child: Text(
                                    '* ไม่มีข้อมูล *',
                                    style: TextStyle(
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            ),
                          ]),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        )),
      ],
    );
  }
}

class Tableclasstable extends StatelessWidget {
  const Tableclasstable({super.key});

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return Column(
      children: <Widget>[
        Container(
          alignment: Alignment.center,
          child: Image.network(
            'lib/imags/reg.png',
            width: double.infinity,
            fit: BoxFit.cover,
          ),
        ),
        const SizedBox(
          height: 5,
        ),
        const Text(
          'ตารางเรียน/สอบของรายวิชาที่ลงทะเบียนไว้แล้ว',
          style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: Colors.deepOrange),
        ),
        Expanded(
          flex: 1,
          child: SizedBox(
            width: double.infinity,
            child: GridView.count(
              primary: false,
              padding: const EdgeInsets.all(20),
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              crossAxisCount: 1,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: screenWidth * 0.1,
                    vertical: screenHeight * 0.1,
                  ),
                  child: Container(
                    width: 300,
                    height: screenHeight,
                    decoration: BoxDecoration(
                      color: Colors.grey[300],
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        const Text(
                          "ตารางเรียน",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        Container(
                          margin: EdgeInsets.all(20),
                          child: Table(
                            columnWidths: const {
                              0: FlexColumnWidth(4),
                              1: FlexColumnWidth(8),
                              2: FlexColumnWidth(8),
                              3: FlexColumnWidth(8),
                            },
                            defaultColumnWidth: const FixedColumnWidth(70.0),
                            border: TableBorder.all(
                                color: Colors.white,
                                style: BorderStyle.solid,
                                width: 1),
                            children: [
                              TableRow(children: [
                                Container(
                                  height: 20,
                                  color: Colors.grey[850],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      'Day/Time',
                                      style: TextStyle(
                                          fontSize: 8.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white70),
                                    ),
                                  ]),
                                ),
                                Container(
                                  height: 20,
                                  color: Colors.grey[850],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text('10:00-12:00',
                                        style: TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                                Container(
                                  height: 20,
                                  color: Colors.grey[850],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text('13:00-16:00	',
                                        style: TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                                Container(
                                  height: 20,
                                  color: Colors.grey[850],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text('17:00-19:00	',
                                        style: TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                              ]),
                              TableRow(children: [
                                Container(
                                  height: 40,
                                  color: Colors.grey,
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(' จันทร์',
                                        style: TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                                Container(
                                  height: 40,
                                  color: Colors.indigo[200],
                                  child: Column(children: const [
                                    Text('88624559-59',
                                        style: TextStyle(fontSize: 10.0)),
                                    Text('(3) 2, IF-4M210',
                                        style: TextStyle(fontSize: 10.0)),
                                    Text('IF',
                                        style: TextStyle(fontSize: 10.0)),
                                  ]),
                                ),
                                Column(children: const [Text('')]),
                                Column(children: const [Text('')]),
                              ]),
                              TableRow(children: [
                                Container(
                                  height: 40,
                                  color: Colors.grey,
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(' อังคาร',
                                        style: TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                                Column(children: const [Text('')]),
                                Column(children: const [Text('')]),
                                Container(
                                  height: 40,
                                  color: Colors.indigo[200],
                                  child: Column(children: const [
                                    Text('88624459-59',
                                        style: TextStyle(fontSize: 10.0)),
                                    Text('(3) 2, IF-3C01',
                                        style: TextStyle(fontSize: 10.0)),
                                    Text('IF',
                                        style: TextStyle(fontSize: 10.0)),
                                  ]),
                                ),
                              ]),
                              TableRow(children: [
                                Container(
                                  height: 40,
                                  color: Colors.grey,
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(' พุธ',
                                        style: TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                                Column(children: const [Text('')]),
                                Container(
                                  height: 40,
                                  color: Colors.indigo[200],
                                  child: Column(children: const [
                                    Text('88624459-59',
                                        style: TextStyle(fontSize: 10.0)),
                                    Text('(3) 2, IF-3C01',
                                        style: TextStyle(fontSize: 10.0)),
                                    Text('IF',
                                        style: TextStyle(fontSize: 10.0)),
                                  ]),
                                ),
                                Column(children: [Text('')]),
                              ]),
                              TableRow(children: [
                                Container(
                                  height: 40,
                                  color: Colors.grey,
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(' พฤหัสบดี',
                                        style: TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                                Container(
                                  height: 40,
                                  color: Colors.indigo[200],
                                  child: Column(children: const [
                                    Text('88624559-59',
                                        style: TextStyle(fontSize: 10.0)),
                                    Text('(3) 2, IF-4M210',
                                        style: TextStyle(fontSize: 10.0)),
                                    Text('IF',
                                        style: TextStyle(fontSize: 10.0)),
                                  ]),
                                ),
                                Container(
                                  height: 40,
                                  color: Colors.indigo[200],
                                  child: Column(children: const [
                                    Text('88624559-59',
                                        style: TextStyle(fontSize: 10.0)),
                                    Text('(3) 2, IF-4M210',
                                        style: TextStyle(fontSize: 10.0)),
                                    Text('IF',
                                        style: TextStyle(fontSize: 10.0)),
                                  ]),
                                ),
                                Column(children: const [Text('')]),
                              ]),
                              TableRow(children: [
                                Container(
                                  height: 40,
                                  color: Colors.grey,
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(' ศุกร์',
                                        style: TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                                Column(children: const [Text('')]),
                                Column(children: const [Text('')]),
                                Container(
                                  height: 40,
                                  color: Colors.indigo[200],
                                  child: Column(children: const [
                                    Text('88624459-59',
                                        style: TextStyle(fontSize: 10.0)),
                                    Text('(3) 2, IF-3C01',
                                        style: TextStyle(fontSize: 10.0)),
                                    Text('IF',
                                        style: TextStyle(fontSize: 10.0)),
                                  ]),
                                ),
                              ]),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: screenWidth * 0.1,
                    vertical: screenHeight * 0.1,
                  ),
                  child: Container(
                    width: 300,
                    height: screenHeight,
                    decoration: BoxDecoration(
                      color: Colors.grey[300],
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        const Text(
                          "ตารางสอบ",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        Container(
                          color: Colors.white60,
                          margin: EdgeInsets.all(20),
                          child: Table(
                            columnWidths: const {
                              0: FlexColumnWidth(1),
                              1: FlexColumnWidth(2),
                              2: FlexColumnWidth(1),
                              // 3: FlexColumnWidth(1),
                              // 4: FlexColumnWidth(1),
                            },
                            defaultColumnWidth: FixedColumnWidth(70.0),
                            border: TableBorder.all(
                                color: Colors.white60,
                                style: BorderStyle.solid,
                                width: 1),
                            children: [
                              TableRow(children: [
                                Container(
                                  height: 20,
                                  color: Colors.grey[300],
                                  child: Column(children: [
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      ' รหัสวิชา',
                                      style: TextStyle(
                                          fontSize: 9.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.blue[900]),
                                    ),
                                  ]),
                                ),
                                Container(
                                  height: 20,
                                  color: Colors.grey[300],
                                  child: Column(children: [
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Text(' ชื่อรายวิชา',
                                        style: TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.blue[900]))
                                  ]),
                                ),
                                Container(
                                  height: 20,
                                  color: Colors.grey[300],
                                  child: Column(children: [
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Text(' กลุ่ม',
                                        style: TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.blue[900]))
                                  ]),
                                ),
                                Container(
                                  height: 20,
                                  color: Colors.grey[300],
                                  child: Column(children: [
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Text(' สอบกลางภาค',
                                        style: TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.blue[900]))
                                  ]),
                                ),
                                Container(
                                  height: 20,
                                  color: Colors.grey[300],
                                  child: Column(children: [
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Text(' สอบปลายภาค',
                                        style: TextStyle(
                                            fontSize: 9.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.blue[900]))
                                  ]),
                                ),
                              ]),
                              TableRow(children: [
                                Container(
                                  // height: 40,
                                  // color: Colors.grey,
                                  child: Column(children: [
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Text(' 88624459-59',
                                        style: TextStyle(
                                            fontSize: 5.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black))
                                  ]),
                                ),
                                Container(
                                  // height: 40,
                                  // color: Colors.indigo[200],
                                  child: Column(children: const [
                                    Text(
                                        ' Object-Oriented Analysis and Design' +
                                            '\n' +
                                            ' การวิเคราะห์และออกแบบเชิงวัตถุ',
                                        style: TextStyle(
                                            fontSize: 5.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black)),
                                  ]),
                                ),
                                Column(children: const [
                                  Text('2',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [
                                  Text(
                                      ' 17 ม.ค. 2566 (C)' +
                                          '\n' +
                                          ' เวลา 17:00-20:00' '\n' +
                                          '       IF-11M280',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [Text('-')]),
                              ]),
                              TableRow(children: [
                                Column(children: const [
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(' 88646259-59',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [
                                  Text(
                                      ' Introduction to Natural Language Processing' +
                                          '\n' +
                                          ' การประมวลผลภาษาธรรมชาติเบื้องต้น',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black)),
                                ]),
                                Column(children: const [
                                  Text('2',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [
                                  Text(
                                      ' 18 ม.ค. 2566 (C)' +
                                          '\n' +
                                          ' เวลา 17:00-20:00' '\n' +
                                          '       IF-11M280',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [Text('-')]),
                              ]),
                              TableRow(children: [
                                Column(children: const [
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(' 88624459-59',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [
                                  Text(
                                      ' Object-Oriented Analysis and Design' +
                                          '\n' +
                                          ' การวิเคราะห์และออกแบบเชิงวัตถุ',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black)),
                                ]),
                                Column(children: const [
                                  Text('2',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [
                                  Text(
                                      ' 17 ม.ค. 2566 (C)' +
                                          '\n' +
                                          ' เวลา 17:00-20:00' '\n' +
                                          '       IF-11M280',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [Text('-')]),
                              ]),
                              TableRow(children: [
                                Column(children: const [
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(' 88634259-59 ',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Container(
                                  // height: 40,
                                  // color: Colors.indigo[200],
                                  child: Column(children: const [
                                    Text(
                                        ' Introduction to Natural Language Processing' +
                                            '\n' +
                                            ' การประมวลผลภาษาธรรมชาติเบื้องต้น',
                                        style: TextStyle(
                                            fontSize: 5.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black)),
                                  ]),
                                ),
                                Column(children: const [
                                  Text('2',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [
                                  Text(
                                      ' 18 ม.ค. 2566 (C)' +
                                          '\n' +
                                          ' เวลา 17:00-20:00' '\n' +
                                          '       IF-11M280',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: [Text('-')]),
                              ]),
                              TableRow(children: [
                                Column(children: const [
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(' 88624459-59',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [
                                  Text(
                                      ' Object-Oriented Analysis and Design' +
                                          '\n' +
                                          ' การวิเคราะห์และออกแบบเชิงวัตถุ',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black)),
                                ]),
                                Column(children: const [
                                  Text('2',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [
                                  Text(
                                      ' 17 ม.ค. 2566 (C)' +
                                          '\n' +
                                          ' เวลา 17:00-20:00' '\n' +
                                          '       IF-11M280',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [Text('-')]),
                              ]),
                              TableRow(children: [
                                Column(children: const [
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(' 88634259-59 ',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: const [
                                  Text(
                                      ' Introduction to Natural Language Processing' +
                                          '\n' +
                                          ' การประมวลผลภาษาธรรมชาติเบื้องต้น',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black)),
                                ]),
                                Column(children: const [
                                  Text('2',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: [
                                  Text(
                                      ' 18 ม.ค. 2566 (C)' +
                                          '\n' +
                                          ' เวลา 17:00-20:00' '\n' +
                                          '       IF-11M280',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: [Text('-')]),
                              ]),
                              TableRow(children: [
                                Container(
                                  // height: 40,
                                  // color: Colors.grey,
                                  child: Column(children: [
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(' 88624459-59',
                                        style: TextStyle(
                                            fontSize: 5.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black))
                                  ]),
                                ),
                                Container(
                                  // height: 40,
                                  // color: Colors.indigo[200],
                                  child: Column(children: [
                                    Text(
                                        ' Object-Oriented Analysis and Design' +
                                            '\n' +
                                            ' การวิเคราะห์และออกแบบเชิงวัตถุ',
                                        style: TextStyle(
                                            fontSize: 5.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black)),
                                  ]),
                                ),
                                Column(children: [
                                  Text('2',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: [
                                  Text(
                                      ' 17 ม.ค. 2566 (C)' +
                                          '\n' +
                                          ' เวลา 17:00-20:00' '\n' +
                                          '       IF-11M280',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ]),
                                Column(children: [Text('-')]),
                              ]),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                    child: ListView(
                  padding: const EdgeInsets.all(8),
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(0)),
                        ),
                        height: 200,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Row(
                              children: const [
                                Text(' ชื่อ             ทักษ์ติโชค อนุมอญ',
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold)),
                              ],
                            ),
                            Row(
                              children: const [
                                Text(' สถานภาพ  กำลังศึกษา',
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold)),
                              ],
                            ),
                            Row(
                              children: const [
                                Text(' คณะ          คณะวิทยาการสารสนเทศ',
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold)),
                                // Text(
                                //     'หลักสูตร  วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติวิทยาศาสตรบัณฑิต (วท.บ.)'),
                              ],
                            ),
                            Row(
                              children: const [
                                Text(
                                  ' หลักสูตร    วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ' +
                                      '\n' +
                                      '                   วิทยาศาสตรบัณฑิต (วท.บ.)',
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                            Row(
                              children: const [
                                Text(
                                  ' อ.ที่ปรึกษา ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม',
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                            Container(
                              margin: EdgeInsets.all(20),
                              child: Table(
                                columnWidths: const {
                                  0: FlexColumnWidth(1),
                                  1: FlexColumnWidth(1),
                                },
                                defaultColumnWidth: FixedColumnWidth(70.0),
                                border: TableBorder.all(
                                  color: Colors.black,
                                  style: BorderStyle.solid,
                                  width: 1,
                                ),
                                children: [
                                  TableRow(children: [
                                    TableCell(
                                      child: SizedBox(
                                        height: 15,
                                        child: Container(
                                          color: Colors.indigo[900],
                                          child: Column(children: const [
                                            Text('Thesis',
                                                style: TextStyle(
                                                    fontSize: 10.0,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white)),
                                          ]),
                                        ),
                                      ),
                                    ),
                                  ]),
                                  const TableRow(children: [
                                    TableCell(
                                      child: SizedBox(
                                        height: 30,
                                        child: Center(
                                          child: Text(
                                            '* ไม่มีข้อมูล *',
                                            style: TextStyle(
                                                fontSize: 12.0,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ]),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                )),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
