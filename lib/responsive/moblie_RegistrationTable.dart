import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';

import 'listname_table.dart';
import 'moblie_Home.dart';
import 'moblie_Record.dart';
import 'moblie_Tableclass.dart';

class RegistrationTables extends StatelessWidget {
  const RegistrationTables({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          alignment: Alignment.center,
          child: Image.network(
            'lib/imags/reg.png',
            width: double.infinity,
            fit: BoxFit.cover,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Container(
          child: Text(
            'ผลลงทะเบียน',
            style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: Colors.deepOrange),
          ),
        ),
        Expanded(
          child: ListView(
            children: [
              SafeArea(
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: RegistrationTable(),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class RegistrationTablestable extends StatelessWidget {
  const RegistrationTablestable({super.key});

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return Column(
      children: <Widget>[
        Container(
          alignment: Alignment.center,
          child: Image.network(
            'lib/imags/reg.png',
            width: double.infinity,
            fit: BoxFit.cover,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Container(
          child: Text(
            'ผลลงทะเบียน',
            style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: Colors.deepOrange),
          ),
        ),
        Expanded(
          child: ListView(
            children: [
              SafeArea(
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: RegistrationTable(),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class RegistrationEdit extends StatelessWidget {
  const RegistrationEdit({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          alignment: Alignment.center,
          child: Image.network(
            'lib/imags/reg.png',
            width: double.infinity,
            fit: BoxFit.cover,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Container(
          child: Text(
            'แก้ไข',
            style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: Colors.deepOrange),
          ),
        ),
        // SafeArea(
        //   child: Padding(
        //     padding: EdgeInsets.all(8.0),
        //     child: RegistrationTable(),
        //   ),
        // ),
      ],
    );
  }
}

class RegistrationHistory extends StatelessWidget {
  const RegistrationHistory({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          alignment: Alignment.center,
          child: Image.network(
            'lib/imags/reg.png',
            width: double.infinity,
            fit: BoxFit.cover,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Container(
          child: Text(
            'ประวัติทำรายการ',
            style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: Colors.deepOrange),
          ),
        ),
        // SafeArea(
        //   child: Padding(
        //     padding: EdgeInsets.all(8.0),
        //     child: RegistrationTable(),
        //   ),
        // ),
      ],
    );
  }
}
