import 'dart:html';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:midterm_exam/responsive/moblie_Home.dart';
import 'package:midterm_exam/responsive/moblie_Tableclass.dart';
import '../main.dart';
import '../util/utilities.dart';
import '../constants.dart';

class Petition extends StatefulWidget {
  const Petition({Key? key}) : super(key: key);

  @override
  State<Petition> createState() => _PetitionState();
}

class _PetitionState extends State<Petition> {
  int currentIndex = 1;
  List widfgetOptions = [
    const Tableclass(),
    const Home(),
    const Text('ตารางสอนอาจาร์ย'),
  ];
  String dropdownvalue = 'เลือนค่าเทอม';
  var items = [
    'เลือนค่าเทอม',
    'พักการเรียน',
  ];
  TextEditingController textarea = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(' ยื่นคำร้อง'),
        backgroundColor: Colors.grey[900],
      ),
      backgroundColor: const Color.fromARGB(255, 235, 236, 217),
      body: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            child: Image.network(
              'lib/imags/reg.png',
              width: double.infinity,
              fit: BoxFit.cover,
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          Container(
            child: const Text(
              ' คำร้อง',
              style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  color: Colors.deepOrange),
            ),
          ),
          Expanded(
            child: ListView(
              children: [
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Container(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                    ),
                    height: 550,
                    child: Column(
                      // mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Row(
                          children: [
                            Image.network(
                              'lib/imags/cercuit_1.gif',
                              // fit: BoxFit.contain,
                            ),
                            const Text(' จันทร์, 26 มกราคม 2023',
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold)),
                          ],
                        ),
                        Row(
                          children: [
                            Image.network(
                              'lib/imags/cercuit_1.gif',
                              // fit: BoxFit.contain,
                            ),
                            const Text(' ขั้นที่ 1 เลือก ชนิดคำขอ',
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold)),
                            const SizedBox(
                              width: 5,
                            ),
                            DropdownButton(
                              // Initial Value
                              value: dropdownvalue,

                              // Down Arrow Icon
                              icon: const Icon(Icons.keyboard_arrow_down),

                              // Array list of items
                              items: items.map((String items) {
                                return DropdownMenuItem(
                                  value: items,
                                  child: Text(items),
                                );
                              }).toList(),
                              // After selecting the desired option,it will
                              // change button value to selected value
                              onChanged: (String? newValue) {
                                setState(() {
                                  dropdownvalue = newValue!;
                                });
                              },
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Image.network(
                              'lib/imags/cercuit_1.gif',
                              // fit: BoxFit.contain,
                            ),
                            const Text(' ขั้นที่ 2 ระบุปีการศึกษา2565  / ',
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold)),
                            const Text(
                              '1 ',
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.blueAccent,
                                  decoration: TextDecoration.underline),
                            ),
                            const Text('2 ',
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.blueAccent,
                                    decoration: TextDecoration.underline)),
                            const Text('ฤดุร้อน',
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.blueAccent,
                                    decoration: TextDecoration.underline)),
                          ],
                        ),
                        Row(
                          children: [
                            Image.network(
                              'lib/imags/cercuit_1.gif',
                              // fit: BoxFit.contain,
                            ),
                            const Text(
                                ' ขั้นที่ 3 กรอกข้อความคำร้องลงในช่องว่าง' +
                                    '\n' +
                                    ' (ไม่เกิน 100 ตัวอักษร)',
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold)),
                          ],
                        ),
                        Container(
                          alignment: Alignment.center,
                          padding: const EdgeInsets.all(20),
                          child: Column(
                            children: [
                              SizedBox(
                                height: 100.0,
                                child: TextField(
                                  controller: textarea,
                                  keyboardType: TextInputType.multiline,
                                  maxLines: 4,
                                  decoration: const InputDecoration(
                                      hintText: "",
                                      focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1,
                                              color: Colors.redAccent))),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Row(
                          children: <Widget>[
                            const Text(' จำนวนตัวอักษร',
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold)),
                            const SizedBox(
                              width: 10,
                            ),
                            const Flexible(
                              child: SizedBox(
                                width: 80.0,
                                height: 50.0,
                                child: TextField(
                                  textAlign: TextAlign.center,
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: '0',
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            ElevatedButton(
                                onPressed: () {
                                  print(textarea.text);
                                },
                                child: const Text("นับจำนวนตัวอักษร")),
                            const SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Image.network(
                              'lib/imags/cercuit_1.gif',
                              // fit: BoxFit.contain,
                            ),
                            const Text(' ขั้นที่ 4 กดปุ่ม',
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold)),
                            const SizedBox(
                              width: 5,
                            ),
                            ElevatedButton(
                                onPressed: () {
                                  print(textarea.text);
                                },
                                child: const Text("บันทึก")),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        SizedBox(
                          height: 100,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Row(
                                children: [
                                  SizedBox(
                                    width: 350,
                                    child:
                                        Image.network('lib/imags/horz_1.gif'),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  Text(
                                      ' 63160005 : นายทักษ์ติโชค อนุมอญ : คณะวิทยาการสารสนเทศ',
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.indigo[900])),
                                ],
                              ),
                              Row(
                                children: const [
                                  Text(
                                      ' หลักสูตร: 2115020: วท.บ. (วิทยาการคอมพิวเตอร์)',
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold)),
                                ],
                              ),
                              Row(
                                children: const [
                                  Text(
                                      ' ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ : วิชาโท: 0: -',
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold)),
                                ],
                              ),
                              Row(
                                children: const [
                                  Text(
                                    ' สถานภาพ: กำลังศึกษา',
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              Row(
                                children: const [
                                  Text(
                                    ' อ.ที่ปรึกษา ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม',
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: 150,
                      child: FloatingActionButton.extended(
                        backgroundColor: Colors.red[900],
                        onPressed: () {},
                        label: const Text(
                          "ติดตามคำขอร้อง",
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
