import 'dart:html';
import 'package:flutter/material.dart';
import 'package:midterm_exam/responsive/moblie_Home.dart';
import 'package:midterm_exam/responsive/moblie_Tableclass.dart';
import '../main.dart';
import '../util/utilities.dart';
import '../constants.dart';

class Approval extends StatefulWidget {
  const Approval({Key? key}) : super(key: key);

  @override
  State<Approval> createState() => _ApprovalState();
}

class _ApprovalState extends State<Approval> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(' ผลการอนุมัติลงทะเบียน'),
        backgroundColor: Colors.grey[900],
      ),
      backgroundColor: const Color.fromARGB(255, 235, 236, 217),
      body: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            child: Image.network(
              'lib/imags/reg.png',
              width: double.infinity,
              fit: BoxFit.cover,
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          const Text(
            'ผลการอนุมัติลงทะเบียน',
            style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: Colors.deepOrange),
          ),
          Expanded(
            child: ListView(
              children: [
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Container(
                    // width: 00,
                    height: 500,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.all(20),
                          child: Table(
                            columnWidths: const {
                              0: FlexColumnWidth(6),
                              1: FlexColumnWidth(6),
                              2: FlexColumnWidth(6),
                              3: FlexColumnWidth(6),
                              4: FlexColumnWidth(8),
                              5: FlexColumnWidth(8),
                              6: FlexColumnWidth(8),
                              7: FlexColumnWidth(8),
                              8: FlexColumnWidth(8),
                              9: FlexColumnWidth(8),
                              10: FlexColumnWidth(8),
                            },
                            defaultColumnWidth: FixedColumnWidth(70.0),
                            border: TableBorder.all(
                                color: Colors.white,
                                style: BorderStyle.solid,
                                width: 1),
                            children: [
                              TableRow(children: [
                                Container(
                                  height: 20,
                                  color: Colors.deepPurple[400],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      ' วันที่',
                                      style: TextStyle(
                                          fontSize: 5.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white70),
                                    ),
                                  ]),
                                ),
                                Container(
                                  height: 20,
                                  color: Colors.deepPurple[400],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(' รหัสวิชา',
                                        style: TextStyle(
                                            fontSize: 5.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                                Container(
                                  height: 20,
                                  color: Colors.deepPurple[400],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(' ชื่อรายวิชา',
                                        style: TextStyle(
                                            fontSize: 5.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                                Container(
                                  height: 20,
                                  color: Colors.deepPurple[400],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(' หน่วยกิต',
                                        style: TextStyle(
                                            fontSize: 5.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                                Container(
                                  height: 20,
                                  color: Colors.deepPurple[400],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(' กลุ่ม',
                                        style: TextStyle(
                                            fontSize: 5.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                                Container(
                                  height: 20,
                                  color: Colors.deepPurple[400],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(' แบบ',
                                        style: TextStyle(
                                            fontSize: 5.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                                Container(
                                  height: 20,
                                  color: Colors.deepPurple[400],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(' อาจารย์ที่ปรึกษา',
                                        style: TextStyle(
                                            fontSize: 5.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                                Container(
                                  height: 20,
                                  color: Colors.deepPurple[400],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(' อาจารย์/ผู้สอน',
                                        style: TextStyle(
                                            fontSize: 5.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                                Container(
                                  height: 20,
                                  color: Colors.deepPurple[400],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(' คณบดี',
                                        style: TextStyle(
                                            fontSize: 5.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                                Container(
                                  height: 20,
                                  color: Colors.deepPurple[400],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(' ผลการอนุมัติ',
                                        style: TextStyle(
                                            fontSize: 5.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                                Container(
                                  height: 20,
                                  color: Colors.deepPurple[400],
                                  child: Column(children: const [
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(' หมายเหตุ',
                                        style: TextStyle(
                                            fontSize: 5.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white70))
                                  ]),
                                ),
                              ]),
                            ],
                          ),
                        ),
                        Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: const [
                              Text(
                                ' นิสิตจะสามารถ',
                                style:
                                    TextStyle(color: Colors.red, fontSize: 8),
                              ),
                              Text('เพิ่ม-ลดรายวิชาได้อีกครั้ง',
                                  style: TextStyle(
                                      color: Colors.green, fontSize: 8)),
                              Text(
                                ' เมื่อผลการอนุมัติครบทุกรายการ และยังอยู่ในช่วงเพิ่ม-ลด',
                                style:
                                    TextStyle(color: Colors.red, fontSize: 8),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 200,
                        ),
                        Center(
                          child: Card(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    const Text(' ปีการศึกษา'),
                                    TextButton(
                                      child: Image.network(
                                          'lib/imags/pull_left_1.gif'),
                                      onPressed: () {/* ... */},
                                    ),
                                    const Text('2564'),
                                    // const SizedBox(width: 8),
                                    TextButton(
                                      child: Image.network(
                                          'lib/imags/pull_right_1.gif'),
                                      onPressed: () {/* ... */},
                                    ),
                                    const Text('/ '),
                                    const Text(' 1'),
                                    SizedBox(
                                      width: 2,
                                      child: TextButton(
                                        child: const Text(
                                          '2',
                                          style: TextStyle(
                                              color: Colors.blueAccent,
                                              decoration:
                                                  TextDecoration.underline),
                                        ),
                                        onPressed: () {/* ... */},
                                      ),
                                    ),
                                    SizedBox(
                                      width: 100,
                                      child: TextButton(
                                        child: const Text(
                                          'ฤดูร้อน',
                                          style: TextStyle(
                                            fontSize: 14,
                                            color: Colors.blueAccent,
                                            decoration:
                                                TextDecoration.underline,
                                          ),
                                        ),
                                        onPressed: () {/* ... */},
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                Container(
                                  decoration: const BoxDecoration(
                                    color: Colors.white,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(0)),
                                  ),
                                  height: 150,
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      Row(
                                        children: [
                                          SizedBox(
                                            width: 350,
                                            child: Image.network(
                                                'lib/imags/horz_1.gif'),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                              ' 63160005 : นายทักษ์ติโชค อนุมอญ : คณะวิทยาการสารสนเทศ',
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.indigo[900])),
                                        ],
                                      ),
                                      Row(
                                        children: const [
                                          Text(
                                              ' หลักสูตร: 2115020: วท.บ. (วิทยาการคอมพิวเตอร์)',
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.bold)),
                                        ],
                                      ),
                                      Row(
                                        children: const [
                                          Text(
                                              ' ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ : วิชาโท: 0: -',
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.bold)),
                                        ],
                                      ),
                                      Row(
                                        children: const [
                                          Text(
                                            ' สถานภาพ: กำลังศึกษา',
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: const [
                                          Text(
                                            ' อ.ที่ปรึกษา ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม',
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
