import 'package:flutter/material.dart';

import 'mobile_main.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ConstrainedBox(
        constraints: const BoxConstraints(maxWidth: 300.0),
        child: Expanded(
          child: ListView(
            children: [
              Column(
                children: [
                  const SizedBox(height: 50.0),
                  Image.network(
                    'lib/imags/key.gif',
                    alignment: Alignment.center,
                  ),
                  const SizedBox(height: 100.0),
                  const TextField(
                      decoration: InputDecoration(labelText: 'รหัสประจำตัว')),
                  const TextField(
                      decoration: InputDecoration(labelText: 'รหัสผ่าน')),
                  const SizedBox(height: 30.0),
                  ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const MobileScaffold()),
                        );
                      },
                      child: const Text('ตรวจสอบ')),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
