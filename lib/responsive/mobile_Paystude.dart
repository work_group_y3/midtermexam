import 'dart:html';

import 'package:flutter/material.dart';
import 'package:midterm_exam/responsive/moblie_Home.dart';
import 'package:midterm_exam/responsive/moblie_Tableclass.dart';
import '../main.dart';
import '../util/utilities.dart';
import '../constants.dart';

class Paystude extends StatefulWidget {
  const Paystude({Key? key}) : super(key: key);

  @override
  State<Paystude> createState() => _PaystudeState();
}

class _PaystudeState extends State<Paystude> {
  int currentIndex = 1;
  List widfgetOptions = [
    const Tableclass(),
    const Home(),
    const Text('ตารางสอนอาจาร์ย'),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(' ภาระค่าใช้จ่าย'),
        backgroundColor: Colors.grey[900],
      ),
      backgroundColor: const Color.fromARGB(255, 235, 236, 217),
      body: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            child: Image.network(
              'lib/imags/reg.png',
              width: double.infinity,
              fit: BoxFit.cover,
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          const Text(
            'ภาระค่าใช้จ่าย/ทุนการศึกษา',
            style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: Colors.deepOrange),
          ),
          Expanded(
            child: ListView(
              children: [
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Container(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                    ),
                    // height: 400,
                    child: Column(
                      // mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Row(
                          children: [
                            const Text(
                                ' โปรดเลือกข้อมูลภาระค่าใช้จ่ายที่ต้องการแสดง'
                                '\n'
                                ' 1. ค้างชำระ'
                                '\n'
                                ' 2. ใน ปีการศึกษา',
                                style: TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.bold)),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.grey[300],
                            borderRadius:
                                const BorderRadius.all(Radius.circular(0)),
                          ),
                          height: 100,
                          width: 350,
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  SizedBox(
                                    height: 30,
                                    width: 30,
                                    child: Image.network(
                                        'lib/imags/pull_right_1.gif'),
                                  ),
                                  const Text(
                                    '2563 ',
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  const Text(' / '),
                                  const Text(' 1'),
                                  // ignore: prefer_const_constructors
                                  SizedBox(
                                    width: 2,
                                    child: const Text(
                                      '2',
                                      style: TextStyle(
                                          color: Colors.blueAccent,
                                          decoration: TextDecoration.underline),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                // mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  SizedBox(
                                    height: 30,
                                    width: 30,
                                    child: Image.network(
                                        'lib/imags/pull_right_1.gif'),
                                  ),
                                  const Text(
                                    '2564 ',
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  const Text(' / '),
                                  const Text(' 1'),
                                  const SizedBox(
                                    width: 2,
                                    child: Text(
                                      '2',
                                      style: TextStyle(
                                          color: Colors.blueAccent,
                                          decoration: TextDecoration.underline),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                // mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  SizedBox(
                                    height: 30,
                                    width: 30,
                                    child: Image.network(
                                        'lib/imags/pull_right_1.gif'),
                                  ),
                                  const Text(
                                    '2565 ',
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  const Text(' / '),
                                  const Text(' 1'),
                                  const SizedBox(
                                    width: 2,
                                    child: Text(
                                      '2',
                                      style: TextStyle(
                                          color: Colors.blueAccent,
                                          decoration: TextDecoration.underline),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Row(
                          children: [
                            const Text(' ค้างชำระ',
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold)),
                          ],
                        ),
                        Container(
                          width: 300,
                          height: 100,
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.black,
                              width: 1,
                            ),
                          ),
                          child: const Center(
                              child: Text("ขณะนี้ยังไม่มีรายการค่าใช้จ่าย!",
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold))),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        SizedBox(
                          height: 150,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Row(
                                children: [
                                  SizedBox(
                                    width: 350,
                                    child:
                                        Image.network('lib/imags/horz_1.gif'),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  Text(
                                      ' 63160005 : นายทักษ์ติโชค อนุมอญ : คณะวิทยาการสารสนเทศ',
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.indigo[900])),
                                ],
                              ),
                              Row(
                                children: const [
                                  Text(
                                      ' หลักสูตร: 2115020: วท.บ. (วิทยาการคอมพิวเตอร์)',
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold)),
                                ],
                              ),
                              Row(
                                children: const [
                                  Text(
                                      ' ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ : วิชาโท: 0: -',
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold)),
                                ],
                              ),
                              Row(
                                children: const [
                                  Text(
                                    ' สถานภาพ: กำลังศึกษา',
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              Row(
                                children: const [
                                  Text(
                                    ' อ.ที่ปรึกษา ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม',
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                width: 150,
                child: FloatingActionButton.extended(
                  backgroundColor: Colors.red[900],
                  onPressed: () {},
                  label: const Text(
                    "ผังบัญชีค่าใช้จ่าย",
                    style: TextStyle(fontSize: 14),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
