import 'package:flutter/material.dart';
import 'package:midterm_exam/responsive/mobile_Approval%20.dart';
import 'package:midterm_exam/responsive/mobile_Paystude.dart';
import 'package:midterm_exam/responsive/moblie_Home.dart';
import 'package:midterm_exam/responsive/moblie_Homeloing.dart';
import 'package:midterm_exam/responsive/moblie_Tableclass.dart';
import '../main.dart';
import '../util/utilities.dart';
import '../constants.dart';
import 'Erorr.dart';
import 'login_page.dart';
import 'mobile_Comment.dart';
import 'mobile_Petition.dart';
import 'moblie_Name.dart';
import 'moblie_Record.dart';
import 'moblie_Registration.dart';
import 'moblie_Results.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  int currentIndex = 1;
  List widfgetOptions = [
    const Tableclass(),
    const LoginPage(),
    const Record(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(' เมนูหลัก'),
        backgroundColor: Colors.grey[900],
      ),
      backgroundColor: const Color.fromARGB(255, 235, 236, 217),
      drawer: Drawer(
        backgroundColor: Colors.grey[300],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            const SizedBox(
              height: 50,
            ),
            Container(
              child: const Text(
                'เมนูหลัก',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: TextButton.icon(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const Homelogin()),
                  );
                },
                icon: const Icon(
                  Icons.home,
                  size: 18.0,
                  color: Colors.black,
                ),
                label: const Text('หน้าหลัก',
                    style: TextStyle(fontSize: 16, color: Colors.black)),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: TextButton.icon(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const Errorlogin()),
                  );
                },
                icon: const Icon(
                  null,
                  size: 18.0,
                  color: Colors.black,
                ),
                label: const Text('วิชาที่เปิดสอน',
                    style: TextStyle(fontSize: 16, color: Colors.black)),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: TextButton.icon(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const Errorlogin()),
                  );
                },
                icon: const Icon(
                  null,
                  size: 18.0,
                  color: Colors.black,
                ),
                label: const Text('ตารางเรียนนิสิต',
                    style: TextStyle(fontSize: 16, color: Colors.black)),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: TextButton.icon(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const Errorlogin()),
                  );
                },
                icon: const Icon(
                  null,
                  size: 18.0,
                  color: Colors.black,
                ),
                label: const Text('ตารางสอนอาจาร์ย',
                    style: TextStyle(fontSize: 16, color: Colors.black)),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: TextButton.icon(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const Errorlogin()),
                  );
                },
                icon: const Icon(
                  null,
                  size: 18.0,
                  color: Colors.black,
                ),
                label: const Text('ตารางการใช้ห้อง',
                    style: TextStyle(fontSize: 16, color: Colors.black)),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: TextButton.icon(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const Errorlogin()),
                  );
                },
                icon: const Icon(
                  null,
                  size: 18.0,
                  color: Colors.black,
                ),
                label: const Text('ปฏิทินการศึกษา',
                    style: TextStyle(fontSize: 16, color: Colors.black)),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: TextButton.icon(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const Errorlogin()),
                  );
                },
                icon: const Icon(
                  null,
                  size: 18.0,
                  color: Colors.black,
                ),
                label: const Text('หลักสูตรที่เปิดสอน',
                    style: TextStyle(fontSize: 16, color: Colors.black)),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: TextButton.icon(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const Errorlogin()),
                  );
                },
                icon: const Icon(
                  null,
                  size: 18.0,
                  color: Colors.black,
                ),
                label: const Text('ผู้สำเร็จการศึกษา',
                    style: TextStyle(fontSize: 16, color: Colors.black)),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: TextButton.icon(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const Errorlogin()),
                  );
                },
                icon: const Icon(
                  null,
                  size: 18.0,
                  color: Colors.black,
                ),
                label: const Text('ตอบคำถาม',
                    style: TextStyle(fontSize: 16, color: Colors.black)),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: TextButton.icon(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const Errorlogin()),
                  );
                },
                icon: const Icon(
                  null,
                  size: 18.0,
                  color: Colors.black,
                ),
                label: const Text('แนะนำการลงทะเบียน',
                    style: TextStyle(fontSize: 16, color: Colors.black)),
              ),
            ),
          ],
        ),
      ),
      body: widfgetOptions[currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(
              icon: Icon(Icons.school), label: 'สำหรับนิสิต'),
          BottomNavigationBarItem(
              icon: Icon(Icons.login), label: 'เข้าสู่ระบบ'),
          BottomNavigationBarItem(
              icon: Icon(Icons.cast_for_education), label: 'สำหรับอาจารย์'),
        ],
        currentIndex: currentIndex,
        onTap: (index) => setState(() => currentIndex = index),
      ),
    );
  }
}
